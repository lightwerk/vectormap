/* global angular,jvm,jQuery */
window.LIGHTWERK = window.LIGHTWERK || {};

(function ($, LW) {
    'use strict';

    LW.Vectormap = {

        /**
         * @param {jQuery} $mapContainer - the div containing the map
         * @param {Object} options - options stores in data-js-options on $mapContainer
         * @param {Array} [entries] - entries stored in data-markers on $container
         * @param {Boolean} mapEnabled - Indicates if the map is enabled after all (if it is
         * not, we still have the list)
         * @param {Boolean} isMultimap
         * @return {void}
         */
        jVectorMap: function ($mapContainer, options, entries, mapEnabled, isMultimap) {

            var multiMap,
                initialMap,
                self = this,
                mainConfig,
                currentMap,
                markers = [],
                regions = {},
                markersEnabled = true;

            entries = entries || [];

            this.mapEnabled = mapEnabled;
            this.init = init;
            this.getMarkersFromEntries = getMarkersFromEntries;
            this.getCallbackDefinition = getCallbackDefinition;
            this.fillInMarkers = fillInMarkers;
            this.adjustJvectormap = adjustJvectormap;
            this.initAngularApp = initAngularApp;
            this.enrichEntriesWithImages = enrichEntriesWithImages;
            this.generateTooltipHtmlForMarker = generateTooltipHtmlForMarker;
            this.generateTooltipHtmlForEntry = generateTooltipHtmlForEntry;
            this.getEntryByUid = getEntryByUid;
            this.selectRegions = selectRegions;
            this.buildRegions = buildRegions;
            this.zoomToRegion = zoomIntoRegion;

            /**
             * @return {void}
             */
            function init () {

                // check for angular
                if (typeof window.angular === 'undefined') {
                    window.console.warn('Vectormap requires AngularJS 1.2.27 or higher to work properly. Angular could not be ' +
                    'found, so the vectormap might not function as expected.');
                    return;
                }

                if (mapEnabled) {
                    // Display of markers can be disabled. This would set
                    // options.markers to false.
                    if (angular.isDefined(options.markers)) {
                        markersEnabled = options.markers;
                    }
                    mainConfig = $.extend({}, options, self.getCallbackDefinition());

                    markers = self.getMarkersFromEntries(entries);
                    self.adjustJvectormap();

                    if (isMultimap) {
                        multiMap = new jvm.MultiMap({
                            container: $mapContainer,
                            maxLevel: 1,
                            main: mainConfig,
                            mapUrlByCode: function(code, multiMap){
                                if ($.inArray(code, initialMap.getSelectedRegions()) === -1) {
                                    throw new Error();
                                }
                                return '/typo3conf/ext/vectormap/Resources/Public/Vendor/Maps/ne_10m_admin_1_states_provinces_' +
                                    code.toLowerCase() + '.js';
                            }
                        });
                        initialMap = multiMap.maps[options.map];
                    } else {
                        $mapContainer.vectorMap(mainConfig);
                        initialMap = $mapContainer.vectorMap('get', 'mapObject');
                    }

                    currentMap = initialMap;

                    self.enrichEntriesWithImages();
                    self.fillInMarkers(markers);
                    self.buildRegions();
                    self.selectRegions(entries);
                }

                self.initAngularApp();

            }

            /**
             * @param {Object|Array} entries
             * @return {Array}
             */
            function getMarkersFromEntries (entries) {
                var markers = [];

                angular.forEach(entries, function (entry) {
                    if (angular.isDefined(entry.markers)) {
                        angular.forEach(entry.markers, function (marker) {
                            marker.entry = entry.uid;
                            markers.push(marker);
                        });
                    }
                });
                return markers;
            }

            /**
             * This method returns the callback definition of the multimap configuration
             * object. It is merged with the rest of the config and passed into the
             * new jvm.MultiMap({main:{}}) method as the main property of the configuration
             * object.
             *
             * @returns {Object}
             */
            function getCallbackDefinition () {
                return {
                    onDrillingCompleted: function (data, map, code) {
                        var remainingEntries = entries.filter(function (entry) {
                                return entry.countryIsoCodeA2 === code || entry.markerCountryCodes.indexOf(code) !== -1;
                            }),
                            remainingMarkers = self.getMarkersFromEntries(remainingEntries),
                            scope = angular.element('.controller-container').scope();

                        currentMap = map;
                        self.fillInMarkers(remainingMarkers, map, code);
                        scope.$apply(function () {
                            scope.region = code;
                            scope.entries = remainingEntries;
                            scope.countryIsoCodeA2 = code;
                        });
                    },
                    onGoingBackCompleted: function (data, map) {
                        var scope = angular.element('.controller-container').scope();
                        currentMap = initialMap;
                        self.fillInMarkers(markers);
                        scope.$apply(function () {
                            scope.region = '';
                            scope.entries = entries;
                            scope.countryIsoCodeA2 = '';
                        });
                        self.selectRegions(entries);
                    },
                    // If a marker tooltip is shown, we basically show our own instead
                    onMarkerTipShow: function (event, tip, code) {
                        var scope = angular.element('.controller-container').scope(),
                            matchingMarkers = scope.markers.filter(function (marker) {
                                return marker.uid === parseInt(code);
                            }),
                            tooltipHtml,
                            persistentTooltip,
                            matchingMarker,
                            nearbyMarkers;
                        if (angular.isArray(matchingMarkers) && matchingMarkers.length) {
                            event.preventDefault();
                            matchingMarker = matchingMarkers[0];
                            tooltipHtml = self.generateTooltipHtmlForMarker(matchingMarker);

                            if (matchingMarker.nearbyMarkers.length) {
                                nearbyMarkers = scope.remainingMarkers.filter(function (marker) {
                                    return matchingMarker.nearbyMarkers.indexOf(marker.uid) !== -1;
                                });
                                if (nearbyMarkers.length) {
                                    angular.forEach(nearbyMarkers, function (nearbyMarker) {
                                        tooltipHtml += '<br />';
                                        tooltipHtml += self.generateTooltipHtmlForMarker(nearbyMarker);
                                    });
                                }
                            }

                            persistentTooltip = angular.element('html').find('.persistent-tooltip');
                            if (persistentTooltip.html() === tooltipHtml) {
                                return;
                            }
                            currentMap.tip.html(tooltipHtml);
                            currentMap.tip.addClass('marker-tip');
                            currentMap.tip.show();
                            currentMap.tipWidth = currentMap.tip.width();
                            currentMap.tipHeight = currentMap.tip.height();
                        }
                    },
                    // If a region tooltip is shown, we remove the marker-tooltip only class first
                    onRegionTipShow: function (event, tip, code) {
                        var persistentTooltip,
                            tooltipHtml = '';
                        currentMap.tip.removeClass('marker-tip');
                        if (angular.isDefined(regions[code])) {
                            angular.forEach(entries, function (entry) {
                                if (entry.regions.indexOf(code) !== -1) {
                                    tooltipHtml += self.generateTooltipHtmlForEntry(entry, true);
                                }
                            });
                            if (tooltipHtml !== '') {
                                tooltipHtml = '<p><strong>' + tip.html() + '</strong></p>' + tooltipHtml;
                                persistentTooltip = angular.element('html').find('.persistent-tooltip');
                                if (persistentTooltip.html() === tooltipHtml) {
                                    return;
                                }

                                currentMap.tip.html(tooltipHtml);
                                currentMap.tip.addClass('marker-tip');
                                currentMap.tip.show();
                                currentMap.tipWidth = currentMap.tip.width();
                                currentMap.tipHeight = currentMap.tip.height();
                            }
                        }
                    },
                    // If a marker is clicked we persist our custom tooltip and add a click event
                    // on anything else to remove it again.
                    onMarkerClick: function (event, code) {
                        currentMap.clearSelectedMarkers();
                        currentMap.setSelectedMarkers(code);
                        var persistentTooltip = currentMap.tip.clone();
                        angular.element('html').find('.persistent-tooltip').remove();
                        angular.element('html').unbind('click');
                        persistentTooltip.addClass('persistent-tooltip');
                        angular.element('body').append(persistentTooltip);
                        currentMap.tip.hide();
                        setTimeout(function () {
                            angular.element('html').bind('click', function () {
                                currentMap.clearSelectedMarkers();
                                angular.element('html').find('.persistent-tooltip').remove();
                            });
                        }, 200);
                        persistentTooltip.on('click', function (event) {
                            event.stopPropagation();
                        });
                    }
                };
            }

            /**
             * @param {Object} marker
             * @returns {string} HTML for Tooltip
             */
            function generateTooltipHtmlForMarker (marker) {
                var tooltipHtml = '';
                if (angular.isDefined(marker.name)) {
                    tooltipHtml += '<p class="marker-name">' + marker.name + '</p>';
                }
                if (angular.isDefined(marker.street) &&
                    angular.isDefined(marker.houseNumber) &&
                    marker.houseNumber !== 0 &&
                    marker.street !== ''
                ) {
                    tooltipHtml += '<p>' + marker.street + ' ' + marker.houseNumber + '</p>';
                } else if (angular.isDefined(marker.street)) {
                    tooltipHtml += '<p>' + marker.street + '</p>';
                }
                if (angular.isDefined(marker.zipCode) &&
                    marker.zipCode !== 0 &&
                    angular.isDefined(marker.city) &&
                    marker.city !== ''
                ) {
                    tooltipHtml += '<p>' + marker.zipCode + ' ' + marker.city + '</p>';
                } else if (angular.isDefined(marker.city)) {
                    tooltipHtml += '<p>' + marker.city + '</p>';
                }
                if (angular.isDefined(marker.entry)) {
                    tooltipHtml += self.generateTooltipHtmlForEntry(self.getEntryByUid(marker.entry), false);
                }
                return tooltipHtml;
            }

            /**
             * @param {Object} entry
             * @param {Boolean} withName
             * @returns {string}
             */
            function generateTooltipHtmlForEntry (entry, withName) {
                var tooltipHtml = '',
                    commentIdentifier;

                if (angular.isDefined(entry.name) && angular.isDefined(withName) && withName) {
                    tooltipHtml += self.generateTooltipHtmlForMarker(entry);
                }

                if (angular.isDefined(entry) && angular.isDefined(entry.electronicAddresses)) {
                    angular.forEach(entry.electronicAddresses, function (electronicAddress) {
                        tooltipHtml += '<p class="electronic-address">';
                        if (electronicAddress.isEmail) {
                            tooltipHtml += '<a href="mailto:' + electronicAddress.identifier + '">' + electronicAddress.identifier + '</a>';
                        } else if (electronicAddress.isUrl) {
                            commentIdentifier = electronicAddress.comment !== '' ? electronicAddress.comment : electronicAddress.identifier;
                            tooltipHtml += '<a href="' + electronicAddress.modifiedIdentifier + '" target="_blank">' + commentIdentifier + '</a>';

                        } else {
                            tooltipHtml += electronicAddress.identifier;
                        }
                        tooltipHtml += '</p>';
                    });
                }
                return tooltipHtml;
            }

            /**
             * @param {Number} uid
             * @returns {Object} Entry
             */
            function getEntryByUid(uid) {
                var matchingEntries = entries.filter(function (entry) {
                    return entry.uid === uid;
                });
                if (angular.isDefined(matchingEntries[0])) {
                    return matchingEntries[0];
                }
            }

            function enrichEntriesWithImages () {
                var $imgContainer = $mapContainer.parent().find('.entry-images'),
                    entryUid,
                    originalImage,
                    entryImages = {},
                    p,
                    entry;
                $imgContainer.find('img').each(function () {
                    entryUid = $(this).data('entryUid');
                    originalImage = $(this).data('originalImageSource');
                    if (angular.isUndefined(entryImages[entryUid])) {
                        entryImages[entryUid] = [];
                    }
                    entryImages[entryUid].push({
                        url: $(this).attr('src'),
                        originalUrl: originalImage,
                        title: $(this).attr('title'),
                        alt: $(this).attr('alt')
                    });
                });
                for (p in entries) {
                    if (entries.hasOwnProperty(p)) {
                        entry = entries[p];
                        if (angular.isDefined(entry.uid) && angular.isDefined(entryImages[entry.uid])) {
                            entry.images = entryImages[entry.uid];
                        }
                    }
                }
            }

            /**
             * @param {Map} map - the map object to add the markers to, initialMap per default
             * @param {Array} markers
             * @param {String} selectedCountry
             * @return {void}
             */
            function fillInMarkers (markers, map, selectedCountry) {
                var amountOfMarkers = markers.length,
                    i = 0,
                    marker,
                    selectedMarkers;

                if (!markersEnabled) {
                    return;
                }

                if (typeof selectedCountry === 'undefined') {
                    selectedCountry = '';
                }

                if (typeof map === "undefined") {
                    map = currentMap;
                }

                selectedMarkers = map.getSelectedMarkers();

                map.removeAllMarkers();
                map.clearSelectedRegions();
                map.clearSelectedMarkers();

                // exit if there are no markers
                if (amountOfMarkers === 0) {
                    return;
                }

                for (i; i < amountOfMarkers; i++) {

                    // only process markers that are inside regions
                    // present on the current map.
                    if (map.params.multiMapLevel === 0 && angular.isUndefined(map.regions[markers[i].countryIsoCodeA2])) {
                        continue;
                    }

                    if (selectedCountry === '' || markers[i].countryIsoCodeA2 === selectedCountry) {
                        marker = {
                            latLng: [markers[i].latitude, markers[i].longitude],
                            name: markers[i].name
                        };
                        map.addMarker(markers[i].uid, marker);
                        if ($.inArray(markers[i].uid.toString(), selectedMarkers) !== -1) {
                            map.setSelectedMarkers(markers[i].uid);
                        }
                    }
                }
            }

            /**
             * We store the IDs of entries that have a region set, to generate a tooltip
             * that holds information to every entry with a certain region.
             *
             * @return {void}
             */
            function buildRegions() {
                angular.forEach(entries, function (entry) {
                    if (angular.isDefined(entry.regionCodes)) {
                        angular.forEach(entry.regionCodes, function (regionCode) {
                            if (angular.isUndefined(regions[regionCode])) {
                                regions[regionCode] = [];
                            }
                            regions[regionCode].push(entry.uid);
                        });
                    }
                });
            }

            /**
             * Goes through the passed entries and adds all found regions to
             * the selected regions of the current map.
             *
             * @param {Array} entries
             * @return {void}
             */
            function selectRegions (entries) {
                var selectedRegions = [],
                    scope;
                // Dont select anything if we have only one country
                if (options.startingPoint === 2) {
                    return;
                }

                // Dont select anything if we are on country level in a multimap
                if (isMultimap && angular.isDefined(currentMap.params.multiMapLevel) && currentMap.params.multiMapLevel === 1) {
                    return;
                }
                scope = angular.element('.controller-container').scope();
                if (angular.isDefined(scope) && scope.countryIsoCodeA2 !== '') {
                    selectedRegions.push(scope.countryIsoCodeA2);
                } else {
                    angular.forEach(entries, function (entry) {
                        if (angular.isDefined(entry.regionCodes)) {
                            angular.forEach(entry.regionCodes, function (regionCode) {
                                if (angular.isDefined(currentMap.regions[regionCode]) && $.inArray(regionCode, selectedRegions) === -1) {
                                    selectedRegions.push(regionCode);
                                }
                            });
                        }
                    });
                }
                currentMap.clearSelectedRegions();
                if (selectedRegions.length) {
                    currentMap.setSelectedRegions(selectedRegions);
                }
            }

            /**
             * Triggers a Zoom to a country. Uses either the
             * countryFilter of the angular app or the build
             * in method of jVectorMap.
             *
             * @param {String} code
             * @return {void}
             */
            function zoomIntoRegion(code) {
                var scope;
                if (angular.isDefined(options.countryFilter) && options.countryFilter) {
                    scope = angular.element('.controller-container').scope();
                    scope.$apply(function () {
                        scope.countryIsoCodeA2 = code;
                        scope.selectCountry(code);
                    });
                } else {
                    currentMap.setFocus({region: code, animate: true});
                }
            }

            /**
             * We adjust the vectorMap library to a certain degree to add new
             * functionality without touching its source code.
             *
             * @return  {void}
             */
            function adjustJvectormap () {
                /*jshint validthis:true */
                this.drillDownWithCallback = drillDownWithCallback;
                this.goBackWithCallback = goBackWithCallback;
                this.createTip = createTip;

                // Add the drillingCompleted callback to hook into the map after
                // a drilling was completed.
                if (jvm.Map.apiEvents.onDrillingCompleted === null || jvm.Map.apiEvents.onDrillingCompleted === undefined) {
                    jvm.Map.apiEvents.onDrillingCompleted = 'drillingCompleted';
                    jvm.Map.apiEvents.onGoingBackCompleted = 'goingBackCompleted';
                    jvm.Map.prototype.createTip = this.createTip;
                    jvm.MultiMap.prototype.drillDown = this.drillDownWithCallback;
                    jvm.MultiMap.prototype.goBack = this.goBackWithCallback;
                }

                /**
                 * This is a copy of the DrillDown function of the jVectorMap library. It has an
                 * additional callback at the end. Since we must not edit the original source code,
                 * we override it.
                 *
                 * Further changes to this method:
                 *   * Early exit if the passed code is not a selected region.
                 *   * The addMap method is called with the full option set from the parent map to keep
                 *     the settings consistent through the levels.
                 *
                 * @param {string} name
                 * @param {string} code
                 * @return {void}
                 */
                function drillDownWithCallback (name, code) {
                    var currentMap = this.history[this.history.length - 1],
                        that = this,
                        focusPromise,
                        downloadPromise,
                        markersOnRegion;

                    // Early exit if not a selected region
                    if ($.inArray(code, currentMap.getSelectedRegions()) === -1) {
                        return;
                    }
                    // No markers at all? Just zoom, don't drill.
                    if (!markersEnabled) {
                        self.zoomToRegion(code);
                        return;
                    }
                    // Are markers on the drilldown map?
                    markersOnRegion = markers.filter(function (marker) {
                        return marker.countryIsoCodeA2 === code;
                    });
                    // No? Just zoom.
                    if (markersOnRegion.length === 0) {
                        self.zoomToRegion(code);
                        return;
                    }

                    focusPromise = currentMap.setFocus({region: code, animate: true});
                    downloadPromise = this.downloadMap(code);
                    focusPromise.then(function(){
                        if (downloadPromise.state() === 'pending') {
                            that.spinner.show();
                        }
                    });
                    downloadPromise.always(function(){
                        that.spinner.hide();
                    });
                    this.drillDownPromise = jvm.$.when(downloadPromise, focusPromise);
                    this.drillDownPromise.then(function(){
                        currentMap.params.container.hide();
                        if (!that.maps[name]) {
                            // pass whole options from parent map
                            that.addMap(name, $.extend(mainConfig, {map: name, multiMapLevel: currentMap.params.multiMapLevel + 1}));
                        } else {
                            that.maps[name].params.container.show();
                        }
                        that.history.push( that.maps[name] );
                        that.backButton.show();
                        // Additional callback
                        currentMap.container.trigger('drillingCompleted.jvectormap', [that.maps[name], code]);
                    });
                }

                function goBackWithCallback () {
                    var currentMap = this.history.pop(),
                        prevMap = this.history[this.history.length - 1],
                        that = this;

                    currentMap.setFocus({scale: 1, x: 0.5, y: 0.5, animate: true}).then(function(){
                        currentMap.params.container.hide();
                        prevMap.params.container.show();
                        prevMap.updateSize();
                        if (that.history.length === 1) {
                            that.backButton.hide();
                        }
                        prevMap.setFocus({scale: 1, x: 0.5, y: 0.5, animate: true});
                        prevMap.container.trigger('goingBackCompleted.jvectormap', prevMap);
                    });
                }

                function createTip () {
                    var map = this;

                    this.tip = jvm.$('<div/>').addClass('jvectormap-tip').appendTo(jvm.$('body'));

                    this.container.mousemove(function (e) {
                        var left = e.pageX + 10,
                            top = e.pageY + 10;

                        if (map.tip.is(':visible')) {
                            map.tip.css({
                                left: left,
                                top: top
                            });
                        }
                    });
                }
            }

            function initAngularApp () {
                angular.module('vectormap', []);
                angular
                    .module('vectormap')
                    .filter('orderObjectBy', orderObjectBy)
                    .controller('VectormapController', VectormapController)
                    .directive('markerCategories', markerCategories)
                    .directive('markerCountries', markerCountries);

                function VectormapController ($scope, $timeout, $sce) {

                    var vm = this,
                        initialized = false,
                        preselectedEntry = 0,
                        tempSearchTerm = '',
                        searchTextTimeout;

                    $scope.hideList = false;
                    $scope.mapEnabled = self.mapEnabled;
                    $scope.activeClass = 0;
                    $scope.selectedCategory = 0;
                    $scope.remainingEntries = [];
                    $scope.remainingMarkers = [];
                    $scope.categories = [];
                    $scope.remainingCategories = [];
                    $scope.countries = [];
                    $scope.remainingCountries = [];
                    $scope.searchText = '';
                    $scope.searchTerm = '';
                    $scope.markers = markers;
                    $scope.entries = entries;
                    $scope.zoomToRegion = zoomToRegion;
                    $scope.zoomToInitialMap = zoomToInitialMap;
                    $scope.selectMarker = selectMarker;
                    $scope.filterByCategory = filterByCategory;
                    $scope.filterBySearchTerm = filterBySearchTerm;
                    $scope.filterByCountry = filterByCountry;
                    $scope.selectCountry = selectCountry;
                    $scope.trust = trust;
                    $scope.region = '';
                    $scope.countryIsoCodeA2 = '';
                    $scope.filter = {
                        countryIsoCodeA2: ''
                    };

                    this.initController = initController;
                    this.zoomToRegion = zoomToRegionOnMap;
                    this.setRemainingCategories = setRemainingCategories;
                    this.setRemainingCountries = setRemainingCountries;
                    this.processRemainingEntries = processRemainingEntries;

                    this.initController();

                    $scope.$watchCollection('remainingEntries', function (remainingEntries) {
                        vm.processRemainingEntries(remainingEntries);
                    });

                    /**
                     * Delay the search execution to preserve performance during typing
                     */
                    $scope.$watch('searchText', function (val) {
                        if (searchTextTimeout) {
                            $timeout.cancel(searchTextTimeout);
                        }
                        tempSearchTerm = val;
                        searchTextTimeout = $timeout(function() {
                            $scope.searchTerm = tempSearchTerm;
                        }, 500);
                    });

                    /**
                     * main function to reflect changes within the set of remaining entries to
                     * display on the map. Is Triggered whenever the collection changes, due to
                     * filtering in ng-repeat (see watcher).
                     *
                     * Triggered additionally after selecting a country, to update marker even if
                     * remainingEntries did not change.
                     *
                     * @param remainingEntries
                     */
                    function processRemainingEntries(remainingEntries) {
                        var result;
                        if (self.mapEnabled && angular.isDefined(remainingEntries) && angular.isDefined(initialMap) && remainingEntries.length) {

                            $scope.remainingMarkers = self.getMarkersFromEntries(remainingEntries);

                            // If the currently selected Entry is not within the remaining Entries (the activeClass property
                            // on $scope would not be found among the entry UIDs in that case) we set the activeClass back to 0
                            // so no item in the list is marked as "active"
                            result = remainingEntries.filter(function (entry) {
                                return entry.uid.toString() === $scope.activeClass.toString();
                            });
                            if (result.length === 0) {
                                $scope.activeClass = 0;
                                currentMap.clearSelectedMarkers();
                            }
                            self.fillInMarkers($scope.remainingMarkers, currentMap, $scope.countryIsoCodeA2);
                            self.selectRegions(remainingEntries);

                            if (!initialized && preselectedEntry > 0) {
                                entries.filter(function (entry) {
                                    if (entry.uid === parseInt(preselectedEntry)) {
                                        $scope.selectMarker(entry.markerUids);
                                    }
                                });
                                initialized = true;
                            } else if (initialized) {
                                $scope.hideList = false;
                            } else {
                                initialized = true;
                            }
                        } else if (remainingEntries.length === 0) {
                            $scope.activeClass = 0;
                            currentMap.clearSelectedMarkers();
                            $scope.remainingMarkers = [];
                            self.fillInMarkers([]);
                            self.selectRegions([]);
                        }

                        vm.setRemainingCategories(remainingEntries);
                        vm.setRemainingCountries(remainingEntries);
                    }

                    /**
                     * Initializes the controller by checking for matching GET parameters
                     *  * tx_vectormap[region] - If this is a valid region key the map zooms to it
                     *  * tx_vectormap[marker] - If this is a valid marker uid, it is selected
                     *  * tx_vectormap[category] - If this is a valid category uid, it is selected
                     *
                     * @return {void}
                     */
                    function initController () {
                        var getParams = getSearchParameters(),
                            property,
                            entryParam,
                            categoryParam,
                            entriesWithThatCategory;
                        $scope.hideList = options.hideListOnStart;

                        if (self.mapEnabled && angular.isDefined(getParams['tx_vectormap[region]']) && angular.isDefined(initialMap.regions[getParams['tx_vectormap[region]']])) {
                            vm.zoomToRegion(getParams['tx_vectormap[region]']);
                        }
                        if (self.mapEnabled && angular.isDefined(getParams['tx_vectormap[entry]']) && markersEnabled) {
                            entryParam = getParams['tx_vectormap[entry]'];
                            for (property in initialMap.markers) {
                                if (initialMap.markers.hasOwnProperty(property)) {
                                    if (property === entryParam) {
                                        preselectedEntry = parseInt(entryParam);
                                        break;
                                    }
                                }
                            }
                        }
                        if (angular.isDefined(getParams['tx_vectormap[category]'])) {
                            categoryParam = parseInt(getParams['tx_vectormap[category]']);
                            entriesWithThatCategory = entries.filter(function (entry) {
                                if (angular.isArray(entry.categoryUids)) {
                                    return entry.categoryUids.indexOf(categoryParam) !== -1;
                                }
                                return false;
                            });
                            if (entriesWithThatCategory.length) {
                                $scope.selectedCategory = categoryParam;
                            }
                            $scope.remainingEntries = entriesWithThatCategory;
                        }

                        $scope.remainingMarkers = markers;

                        /**
                         *
                         * @returns {Object}
                         */
                        function getSearchParameters() {
                            var parameterString = window.location.search.substr(1);
                            return parameterString !== null && parameterString !== '' ? transformToAssocArray(parameterString) : {};
                        }

                        /**
                         * Transforms the query string from getSearchParameters
                         * into an object for better accessing the values.
                         *
                         * @param {string} parameterString
                         * @returns {Object} - {parameter: value}
                         */
                        function transformToAssocArray(parameterString) {
                            var params = {},
                                i = 0,
                                tempArray,
                                parameterArray = decodeURI(parameterString).split('&');
                            for (i; i < parameterArray.length; i++) {
                                tempArray = parameterArray[i].split('=');
                                params[tempArray[0]] = tempArray[1];
                            }
                            return params;
                        }
                    }

                    /**
                     * Waits for the current angular circle to finish and triggers a zoom or a message
                     * in regard of the passed regionCode and the number of markers still shown on
                     * the map.
                     *
                     * @param {string} regionCode - e.g. 'DE'
                     * @return {void}
                     */
                    function zoomToRegion (regionCode) {
                        if (!self.mapEnabled) {
                            return;
                        }
                        $timeout(function () {
                            if (regionCode === '') {
                                $scope.zoomToInitialMap();
                            } else if ($scope.remainingEntries.length > 0) {
                                vm.zoomToRegion(regionCode);
                            }
                            // else: display message that nothing was found
                            // This is done in the template with ng-if="remainingEntries.length === 0"
                        });
                    }

                    /**
                     * Triggers a zoom to the region with the passed region code
                     *
                     * @param {string} regionCode - e.g. 'DE'
                     * @return {void}
                     */
                    function zoomToRegionOnMap (regionCode) {
                        if (!self.mapEnabled) {
                            return;
                        }
                        currentMap.setFocus({region: regionCode, animate: true});
                    }

                    /**
                     * Triggers a zoom to the center of the map on scale 1.
                     *
                     * @return {void}
                     */
                    function zoomToInitialMap () {
                        if (!self.mapEnabled) {
                            return;
                        }
                        if (angular.isDefined(initialMap)) {
                            currentMap.setFocus({scale: 1, x: 0.5, y: 0.5, animate: true});
                        }
                    }

                    /**
                     * Set a marker as selected. As there can only be one selected marker
                     * at a time, we clear any previous selected marker first.
                     * Additionally we zoom in on the marker with a nice transition effect.
                     *
                     * @param {Array} markerIds
                     * @return {void}
                     */
                    function selectMarker (markerIds) {
                        var lat,
                            lng;
                        if (!self.mapEnabled || !markersEnabled) {
                            return;
                        }

                        try {
                            currentMap.clearSelectedMarkers();
                            currentMap.setSelectedMarkers(markerIds);
                        } catch (e) {
                            // If the vectormap is not able to select a marker it is most likely
                            // not currently present on the map, due to filtering applied. This may
                            // happen if a entry has at least one marker and several regions.
                            // We silently discard the error here.
                            return;
                        }

                        if (markerIds.length === 1 && angular.isDefined(currentMap.markers[markerIds[0]])) {
                            lat = currentMap.markers[markerIds[0]].config.latLng[0];
                            lng = currentMap.markers[markerIds[0]].config.latLng[1];
                            currentMap.setFocus({scale: 4, x: 0.5, y: 0.5, animate: true}).then(
                                function () {
                                    currentMap.setFocus({scale: 10, lat: lat, lng: lng, animate: true});
                                }
                            );
                        }
                    }

                    /**
                     * Checks whether or not a passed object contains the current
                     * selected category among its categoryUids.
                     *
                     * This is used for filtering in a ng-repeat.
                     *
                     * @param {Object} object - Could be a entry or a marker
                     * @returns {boolean}
                     */
                    function filterByCategory (object) {
                        if (parseInt($scope.selectedCategory) === 0) {
                            return true;
                        }
                        if (angular.isDefined(object.categoryUids) && angular.isArray(object.categoryUids)) {
                            return object.categoryUids.indexOf(parseInt($scope.selectedCategory)) !== -1;
                        }
                        return false;
                    }

                    /**
                     * Checks whether or not a passed object contains the current
                     * search term among the regarded properties.
                     *
                     * This is used for filtering in a ng-repeat.
                     *
                     * @param {Object} object . Could be a entry or a marker
                     * @returns {boolean}
                     */
                    function filterBySearchTerm (object) {
                        var searchTerm;
                        if ($scope.searchTerm === '') {
                            return true;
                        }
                        searchTerm = $scope.searchTerm.toLowerCase();
                        return object.searchString.toLowerCase().indexOf(searchTerm) > -1;
                    }

                    /**
                     * Checks whether or not a passed object contains the current
                     * country code among the country codes of the markers.
                     *
                     * This is used for filtering in a ng-repeat.
                     *
                     * @param {Object} object . Could be a entry or a marker
                     * @returns {boolean}
                     */
                    function filterByCountry (object) {
                        if ($scope.countryIsoCodeA2 === '') {
                            return true;
                        }
                        if (object.countryIsoCodeA2 === $scope.countryIsoCodeA2) {
                            return true;
                        }
                        if (object.markerCountryCodes.indexOf($scope.countryIsoCodeA2) !== -1) {
                            return true;
                        }
                        if (angular.isDefined(object.regionCodes) && angular.isArray(object.regionCodes) && object.regionCodes.indexOf($scope.countryIsoCodeA2) !== -1) {
                            return true;
                        }
                        if (angular.isDefined(object.regions) && angular.isArray(object.regions) && object.regions.indexOf($scope.countryIsoCodeA2) !== -1) {
                            return true;
                        }
                    }

                    /**
                     * Function is called onChange of the country select box on the list of markers
                     *
                     * @param {String} countryIsoCodeA2
                     * @returns {void}
                     */
                    function selectCountry (countryIsoCodeA2) {
                        if (countryIsoCodeA2 !== '') {
                            self.fillInMarkers(self.getMarkersFromEntries($scope.remainingEntries), currentMap, $scope.countryIsoCodeA2);
                            self.selectRegions($scope.remainingEntries);
                            vm.setRemainingCategories($scope.remainingEntries);
                            $scope.zoomToRegion(countryIsoCodeA2);
                        } else {
                            if (isMultimap && currentMap.params.multiMapLevel > 0) {
                                $timeout(function () {
                                    multiMap.goBack();
                                });
                            } else {
                                $scope.zoomToInitialMap();
                            }
                        }
                    }

                    /**
                     * Changes the set of remaining categories dependent on the remaining markers.
                     * This function is triggered if the remainingEntries change, e.g. on filtering
                     * the list of entries (see the watcher on $scope.remainingEntries).
                     *
                     * @param {Array} remainingEntries - Array of Entry Objects
                     * @returns {void}
                     */
                    function setRemainingCategories (remainingEntries) {
                        var tempArray;
                        if ($scope.searchTerm !== '' || $scope.filter.countryIsoCodeA2 !== '') {
                            $scope.remainingCategories = [];
                            angular.forEach($scope.categories, function (category) {
                                if (parseInt(category.uid) === 0) {
                                    $scope.remainingCategories.push(category);
                                } else {
                                    tempArray = remainingEntries.filter(function (marker) {
                                        if (angular.isArray(marker.categoryUids)) {
                                            return marker.categoryUids.indexOf(category.uid) !== -1;
                                        }
                                        return false;
                                    });
                                    if (tempArray.length) {
                                        $scope.remainingCategories.push(category);
                                    }
                                }
                            });
                        } else {
                            $scope.remainingCategories = $scope.categories;
                        }
                    }

                    /**
                     * Changes the set of remaining countries dependent on the remaining markers.
                     * This function is triggered if the remainingEntries change, e.g. on filtering
                     * the list of markers (see the watcher on $scope.remainingEntries).
                     *
                     * @param {Array} remainingEntries - Array of Entry Objects
                     * @returns {void}
                     */
                    function setRemainingCountries (remainingEntries) {
                        var tempArray;
                        // No search term, but a category
                        if ($scope.searchTerm !== '' || parseInt($scope.selectedCategory) !== 0) {
                            $scope.remainingCountries = [];
                            angular.forEach($scope.countries, function (country) {
                                if (country.isoCodeA2 === '') {
                                    $scope.remainingCountries.push(country);
                                } else {
                                    tempArray = remainingEntries.filter(function (entry) {
                                        return entry.countryIsoCodeA2 === country.isoCodeA2 || entry.markerCountryCodes.indexOf(country.isoCodeA2) !== -1;
                                    });
                                    if (tempArray.length) {
                                        $scope.remainingCountries.push(country);
                                    }
                                }
                            });
                        } else if ($scope.region !== '') {
                            $scope.remainingCountries = [];
                            angular.forEach($scope.countries, function (country) {
                                if (country.isoCodeA2 === $scope.region || country.isoCodeA2 === '') {
                                    $scope.remainingCountries.push(country);
                                }
                            });
                        } else {
                            $scope.remainingCountries = $scope.countries;
                        }
                    }

                    /**
                     * Trusts a passed string as HTML
                     *
                     * @param {String} string
                     * @param {Number} entryId
                     * @returns {String}
                     */
                    function trust (string, entryId) {
                        var matchingEntries;
                        if (string === '') {
                            return '';
                        }
                        matchingEntries = $scope.entries.filter(function (entry) {
                            return entry.uid === entryId;
                        });
                        if (matchingEntries.length && matchingEntries[0].information === string) {
                            return $sce.trustAsHtml(string);
                        }
                        return '';
                    }

                }
                VectormapController.$inject = ['$scope', '$timeout', '$sce'];

                /**
                 * Used as DIRECTIVE marker-categories on the categories
                 * select dropdown filter.
                 *
                 * @returns {Function}
                 */
                function markerCategories () {
                    return function(scope, element, attrs){
                        var categories = element.data('categories') || [],
                            defaultLabel = attrs.defaultLabel;
                        if (angular.isArray(categories)) {
                            categories.unshift({title: defaultLabel, uid: 0});
                        }
                        scope.categories = categories;
                        scope.remainingCategories = categories;
                    };
                }

                /**
                 * Used as DIRECTIVE marker-countries on the country
                 * select dropdown filter.
                 *
                 * @returns {Function}
                 */
                function markerCountries () {
                    return function(scope, element, attrs){
                        var countries = element.data('countries') || [],
                            defaultLabel = attrs.defaultLabel;
                        if (angular.isArray(countries)) {
                            countries.unshift({name: defaultLabel, isoCodeA2: ''});
                        }
                        scope.countries = countries;
                        scope.remainingCountries = countries;
                    };
                }

                function orderObjectBy () {
                    return function (items, field, reverse) {
                        var filtered = [];
                        angular.forEach(items, function (item) {
                            filtered.push(item);
                        });
                        if (filtered.length === 0) {
                            return filtered;
                        }
                        function index (obj, i) {
                            return obj[i];
                        }
                        filtered.sort (function (a, b) {
                            var comparator;
                            var reducedA = field.split('.').reduce(index, a);
                            var reducedB = field.split('.').reduce(index, b);
                            if (reducedA === reducedB) {
                                comparator = 0;
                            } else {
                                comparator = (reducedA > reducedB ? 1 : -1);
                            }
                            return comparator;
                        });
                        if (reverse) {
                            filtered.reverse();
                        }
                        return filtered;
                    };
                }

                angular.bootstrap(document, ['vectormap']);
            }
        }
    };

    $(document).ready(function () {
        $('.tx-vectormap-pi1').each(function (idx, container) {
            var $container = $(container),
                entries = $container.data('entries'),
                $mapContainer = $container.find('.map-container'),
                options,
                vectormap,
                mapEnabled = false,
                isMultimap = true;

            if ($mapContainer.length) {
                mapEnabled = true;
                options = $mapContainer.data('jsOptions');
                // country
                switch (options.startingPoint) {
                    // world
                    case 1:
                        options.map = 'world_mill_en';
                        break;
                    // counry
                    case 2:
                        options.map = options.country + '_' + options.map;
                        isMultimap = false;
                        break;
                    // continential region
                    case 3:
                        options.map = options.continental + '_' + options.map;
                        break;
                    default:
                        throw new Error('Unknown starting Point configured. Check Plugin Options.');
                }
            }

            vectormap = new LW.Vectormap.jVectorMap($mapContainer, options, entries, mapEnabled, isMultimap);
            vectormap.init();

        });
    });

}(jQuery, window.LIGHTWERK));