#!/bin/sh

rm ../*.js

wget --recursive --level=1 http://dev.hovinne.com/jvectormap-maps-builder/maps/ne_10m_admin_1_states_provinces
mv dev.hovinne.com/jvectormap-maps-builder/maps/ne_10m_admin_1_states_provinces/*.js ../.
php replaceMapNames.php

wget https://raw.githubusercontent.com/bjornd/jvectormap/master/converter/converter.py
wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/cultural/ne_110m_admin_0_countries.zip
unzip ne_110m_admin_0_countries.zip
python converter.py world.json

rm -rf converter.py ne_* iso-codes.txt dev.hovinne.com

########
# Notes:
#ogrinfo ne_110m_admin_1_states_provinces.shp ne_110m_admin_1_states_provinces -where "ISO_A2='DE'"
#wget https://raw.githubusercontent.com/jfhovinne/jvectormap-maps-builder/master/bin/iso-codes.txt
#wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/110m/cultural/ne_110m_admin_1_states_provinces.zip
#https://github.com/jfhovinne/jvectormap-maps-builder