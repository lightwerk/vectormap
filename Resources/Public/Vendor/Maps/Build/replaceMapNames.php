<?php

mysql_connect('localhost', 'dev', 'dev') or exit(mysql_error());
mysql_select_db('typo3_62');

$filePaths = glob('../ne_10m_admin_1_states_provinces_*.js');

foreach ($filePaths as $filePath) {
	if (preg_match('/\.\.\/ne_10m_admin_1_states_provinces_([A-Z]{3})\.js/', $filePath, $matches) === FALSE) {
		print($matches);
		exit('File is not valid!');
	}
	$row = mysql_fetch_array(mysql_query('SELECT * FROM static_countries WHERE cn_iso_3 = "' . strtoupper($matches[1]) . '"'), MYSQL_ASSOC);
	if (empty($row)) {
		print_r($matches);
		echo 'Couldn\'t find country in database!';
		continue;
	}
	$content = file_get_contents($filePath);
	$content = str_replace('world_mill_en', strtolower($row['cn_iso_2']) . '_mill_en', $content);
	$targetFilePath = '../ne_10m_admin_1_states_provinces_' . strtolower($row['cn_iso_2']) . '.js';
	if (file_exists($targetFilePath)) {
		print_r($row);
		exit('File exists already: '. $targetFilePath);
	} else {
		file_put_contents($targetFilePath, $content);
		unlink($filePath);
	}
}