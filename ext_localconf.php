<?php

defined('TYPO3_MODE') || exit('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Lightwerk.' . $_EXTKEY,
	'Pi1',
	array(
		'Marker' => 'index',
	),
	array(
		'Marker' => '',
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Lightwerk.' . $_EXTKEY,
	'Pi2',
	array(
		'Marker' => 'select',
	),
	array(
		'Marker' => '',
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
	'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:vectormap/Configuration/TypoScript/PageTSConfig.ts">'
);