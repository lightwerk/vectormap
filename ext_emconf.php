<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Vector Map',
	'description' => 'Vector based world map with markers and a filterable list of marker information',
	'category' => 'plugin',
	'author' => 'Daniel Goerz, Lars Malach',
	'author_email' => 'dlg@lightwerk.com, lm@lightwerk.com',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '2.1.5',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.0',
			'static_info_tables' => '6.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);