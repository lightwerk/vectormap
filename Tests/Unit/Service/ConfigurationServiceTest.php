<?php

namespace Lightwerk\Vectormap\Tests\Unit\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Lightwerk\Vectormap\Service\ConfigurationService;
use TYPO3\CMS\Core\Tests\BaseTestCase;

/**
 * Class ConfigurationServiceTest
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class ConfigurationServiceTest extends BaseTestCase {

	/**
	 * @var array
	 */
	protected $settings = array();

	/**
	 * @var \Lightwerk\Vectormap\Service\ConfigurationService
	 */
	protected $configurationServiceMock;

	/**
	 * @return void
	 */
	public function setUp() {
		$this->settings = array(
			'foo' => 'Bar',
			'map' => array(
				'jsOptions' => array()
			),
			'countryFilter' => array(),
			'categoryFilter' => array(),
			'categories' => array()
		);
		$this->configurationServiceMock = $configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('foo'));
	}

	/**
	 * @return void
	 */
	public function tearDown() {
		$this->settings = NULL;
		$this->configurationServiceMock = NULL;
	}

	/**
	 * @test
	 * @return void
	 */
	public function getSettingsCallsGetModifiedSettings() {
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getModifiedSettings', 'getTypeSecureSettings'));
		$configurationServiceMock
			->expects($this->once())
			->method('getModifiedSettings')
			->will($this->returnValue(array()));
		$configurationServiceMock->getSettings(array());
	}

	/**
	 * @test
	 * @return void
	 */
	public function getSettingsCallsGetModifiedSettingsWithSettings() {
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getModifiedSettings', 'getTypeSecureSettings'));
		$configurationServiceMock
			->expects($this->once())
			->method('getModifiedSettings')
			->with($this->settings)
			->will($this->returnValue(array()));
		$configurationServiceMock->getSettings($this->settings);
	}

	/**
	 * @test
	 * @return void
	 */
	public function getSettingsCallsGetTypeSecureSettings() {
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getModifiedSettings', 'getTypeSecureSettings'));
		$configurationServiceMock
			->method('getModifiedSettings')
			->will($this->returnValue(array()));
		$configurationServiceMock
			->expects($this->once())
			->method('getTypeSecureSettings')
			->will($this->returnValue(array()));
		$configurationServiceMock->getSettings(array());
	}

	/**
	 * @test
	 * @return void
	 */
	public function getSettingsCallsGetTypeSecureSettingsWithReturnValueOfGetModifiedSettings() {
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getModifiedSettings', 'getTypeSecureSettings'));
		$configurationServiceMock
			->method('getModifiedSettings')
			->will($this->returnValue($this->settings));
		$configurationServiceMock
			->expects($this->once())
			->method('getTypeSecureSettings')
			->with($this->settings)
			->will($this->returnValue(array()));
		$configurationServiceMock->getSettings(array());
	}

	/**
	 * @test
	 * @return void
	 */
	public function getModifiedSettingsReturnsArray() {
		$array = $this->configurationServiceMock->_call('getModifiedSettings', $this->settings);
		$this->assertTrue(is_array($array));
	}

	/**
	 * @test
	 * @dataProvider getModifiedSettingsDataProvider
	 * @return void
	 */
	public function getModifiedSettingsDoesNotCallMergeFlexformSettingsIfFlexformIsNotProperSetInSettings($settings) {
		$this->configurationServiceMock
			->expects($this->never())
			->method('mergeFlexformSettings');
		$this->configurationServiceMock->_call('getModifiedSettings', $settings);
	}

	/**
	 * DataProvider for getModifiedSettingsDoesNotCallMergeFlexformSettingsIfFlexformIsNotProperSetInSettings
	 * @return array
	 */
	public function getModifiedSettingsDataProvider() {
		return array(
			'Flexform key not set at all ' => array($this->settings),
			'Flexform key set, but no array' => array(array('flexform' => 'foo')),
			'Flexform key set, but empty array' => array(array('flexform' => array()))
		);
	}

	/**
	 * @return void
	 * @test
	 */
	public function getModifiedSettingsCallsMergeFlexformSettingsIfFlexformIsSetProperlyInSettings() {
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('mergeFlexformSettings'));
		$configurationServiceMock
			->expects($this->once())
			->method('mergeFlexformSettings');
		$configurationServiceMock->_call('getModifiedSettings', array('flexform' => $this->settings));
	}

	/**
	 * @test
	 * @return void
	 */
	public function getModifiedSettingsDisablesCountryFilterIfStartingPointIsCountry() {
		$this->settings['map']['jsOptions']['startingPoint'] = ConfigurationService::STARTING_POINT_COUNTRY;
		$mergedSettings = $this->configurationServiceMock->_call('getModifiedSettings', $this->settings);
		$this->assertEquals(0, $mergedSettings['countryFilter']['enable']);
	}

	/**
	 * @test
	 * @return void
	 */
	public function getModifiedSettingsDisablesCategoryFilterIfListOfCategoriesConTainsNoComma() {
		$this->settings['categoryFilter']['enable'] = 1;
		$this->settings['categories']['enable'] = 1;
		$this->settings['categories']['list'] = '123';
		$mergedSettings = $this->configurationServiceMock->_call('getModifiedSettings', $this->settings);
		$this->assertEquals(0, $mergedSettings['categoryFilter']['enable']);
	}

	/**
	 * @test
	 * @return void
	 */
	public function getModifiedSettingsDisablesNearbyMarkersIfMapIsDisabled() {
		$this->settings['map']['enable'] = 0;
		$this->settings['map']['nearbyMarkers']['enable'] = 1;
		$mergedSettings = $this->configurationServiceMock->_call('getModifiedSettings', $this->settings);
		$this->assertEquals(0, $mergedSettings['map']['nearbyMarkers']['enable']);
	}

	/**
	 * @test
	 * @return void
	 */
	public function getModifiedSettingsDisablesNearbyMarkersIfMarkersAreDisabled() {
		$this->settings['map']['markers']['enable'] = 0;
		$this->settings['map']['nearbyMarkers']['enable'] = 1;
		$mergedSettings = $this->configurationServiceMock->_call('getModifiedSettings', $this->settings);
		$this->assertEquals(0, $mergedSettings['map']['nearbyMarkers']['enable']);
	}

	/**
	 * @test
	 * @return void
	 */
	public function getModifiedSettingsEnrichesJsOptionsWithMarkersEnabledSettings() {
		$this->settings['map']['markers']['enable'] = 1;
		$mergedSettings = $this->configurationServiceMock->_call('getModifiedSettings', $this->settings);
		$this->assertEquals(TRUE, $mergedSettings['map']['jsOptions']['markers']);
	}


	/**
	 * @test
	 * @return void
	 */
	public function mergeFlexformSettingsCallsGetArrayWithoutEmptyValues() {
		$this->settings['flexform'] = array(1,2,3);
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getArrayWithoutEmptyValues'));
		$configurationServiceMock->expects($this->once())->method('getArrayWithoutEmptyValues')->will($this->returnValue($this->settings));
		$configurationServiceMock->_call('mergeFlexformSettings', $this->settings);
	}

	/**
	 * @test
	 * @return void
	 */
	public function mergeFlexformSettingsUnsetsFlexformKey() {
		$this->settings['flexform'] = array(1,2,3);
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getArrayWithoutEmptyValues'));
		$configurationServiceMock->method('getArrayWithoutEmptyValues')->will($this->returnValue(array('foo' => 'bar')));
		$return = $configurationServiceMock->_call('mergeFlexformSettings', $this->settings);
		$this->assertFalse(isset($return['flexform']));
	}

	/**
	 * @test
	 * @return void
	 */
	public function mergeFlexformSettingsMergesReturnOfGetArrayWithoutEmptyValues() {
		$this->settings['flexform'] = array(1,2,3);
		$configurationServiceMock = $this->getAccessibleMock('Lightwerk\Vectormap\Service\ConfigurationService', array('getArrayWithoutEmptyValues'));
		$configurationServiceMock->method('getArrayWithoutEmptyValues')->will($this->returnValue(array('foo' => 'barbar')));
		$return = $configurationServiceMock->_call('mergeFlexformSettings', $this->settings);
		$this->assertEquals($return['foo'], 'barbar');
	}

	/**
	 * @test
	 * @return void
	 */
	public function getArrayWithoutEmptyValuesRemovesEmptyArrays() {
		$arrayWithoutEmptyValues = $this->configurationServiceMock->_call('getArrayWithoutEmptyValues', $this->settings);
		$this->assertEquals($arrayWithoutEmptyValues, array('foo' => 'Bar'));
	}

	/**
	 * @test
	 * @return void
	 */
	public function getArrayWithoutEmptyValuesRemovesEmptyValues() {
		$this->settings['emptyValue'] = '';
		$arrayWithoutEmptyValues = $this->configurationServiceMock->_call('getArrayWithoutEmptyValues', $this->settings);
		$this->assertEquals($arrayWithoutEmptyValues, array('foo' => 'Bar'));
	}

	/**
	 * @test
	 * @return void
	 */
	public function getArrayWithoutEmptyValuesRemovesEmptyValuesInNestedLevels() {
		$this->settings['map']['jsOptions']['emptyValue'] = '';
		$this->settings['map']['jsOptions']['someValue'] = 1;
		$arrayWithoutEmptyValues = $this->configurationServiceMock->_call('getArrayWithoutEmptyValues', $this->settings);
		$this->assertEquals($arrayWithoutEmptyValues, array('foo' => 'Bar', 'map' => array('jsOptions' => array('someValue' => 1))));
	}

	/**
	 * @test
	 * @dataProvider getTypeSecureSettingsDataProvider
	 * @param $settings
	 * @param $expectation
	 * @return void
	 */
	public function getTypeSecureSettingsConvertsValuesToCorrecttTypes($settings, $expectation) {
		$typeSecureSettings = $this->configurationServiceMock->_call('getTypeSecureSettings', $settings);
		$this->assertEquals($typeSecureSettings, $expectation);
	}

	/**
	 * DataProvider for getTypeSecureSettingsConvertsValuesToJavaScriptTypes
	 * @return array
	 */
	public function getTypeSecureSettingsDataProvider() {
		return array(
			'yes to true' => array(
				array('foo' => 'yes'),
				array('foo' => TRUE)
			),
			'yes to true with string conversion' => array(
				array('foo' => 'YES'),
				array('foo' => TRUE)
			),
			'true to true' => array(
				array('foo' => 'true'),
				array('foo' => TRUE)
			),
			'true to true with string conversion' => array(
				array('foo' => 'TrUe'),
				array('foo' => TRUE)
			),
			'no to false' => array(
				array('foo' => 'no'),
				array('foo' => FALSE)
			),
			'no to false with string conversion' => array(
				array('foo' => 'NO'),
				array('foo' => FALSE)
			),
			'false to false' => array(
				array('foo' => 'false'),
				array('foo' => FALSE)
			),
			'false to false with string conversion' => array(
				array('foo' => 'fALsE'),
				array('foo' => FALSE)
			),
			'number string to integer' => array(
				array('foo' => '12'),
				array('foo' => 12)
			),
			'float string to float' => array(
				array('foo' => '12.34'),
				array('foo' => 12.34)
			),
			'nested' => array(
				array(
					'foo' => array(
						'foo' => 'yes',
						'bar' => array(
							'foobar' => '99'
						)
					)
				),
				array(
					'foo' => array(
						'foo' => TRUE,
						'bar' => array(
							'foobar' => 99
						)
					)
				)
			)
		);
	}
}