<?php

namespace Lightwerk\Vectormap\Tests\Unit\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Lightwerk\Vectormap\Service\ConfigurationService;
use TYPO3\CMS\Core\Tests\BaseTestCase;

/**
 * Class CNearbyMarkersServiceTest
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class NearbyMarkersServiceTest extends BaseTestCase {


	/**
	 * @param mixed $testValue
	 * @param boolean $expectation
	 *
	 * @test
	 * @dataProvider transformIntoValidGeoCoordinateDataProvider
	 * @return void
	 */
	public function transformIntoValidGeoCoordinateReturnsValidString($testValue, $expectation) {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('foo'));
		$result = $nearbyMarkersServiceMock->_call('transformIntoValidGeoCoordinate', $testValue);
		$this->assertEquals($expectation, $result);
	}

	/**
	 * DataProvider for transformIntoValidGeoCoordinateValidatesSeventhCharacterFromTheEndToBePeriod
	 * @return array
	 */
	public function transformIntoValidGeoCoordinateDataProvider() {
		return array(
			'Converts to string' => array(4.123456, '4.123456'),
			'Keeps string' => array('4.123456', '4.123456'),
			'Adds righthanded zeros' => array('4.123', '4.123000')
		);
	}

	/**
	 * @test
	 * @return void
	 * @expectedException \Exception
	 */
	public function transformIntoValidGeoCoordinateThrowsExceptionForNonNumberalInput() {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('foo'));
		$nearbyMarkersServiceMock->_call('transformIntoValidGeoCoordinate', 'lalala');
	}

	/**
	 * @test
	 * @return void
	 * @expectedException \Exception
	 */
	public function transformIntoValidGeoCoordinateThrowsExceptionForInputWithoutPeriod() {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('foo'));
		$nearbyMarkersServiceMock->_call('transformIntoValidGeoCoordinate', '1234567');
	}

	/**
	 * @test
	 * @return void
	 */
	public function areGeoCordsNearEachOtherCallsTransformIntoValidGeoCoordinateTwice() {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('transformIntoValidGeoCoordinate', 'transformIntoRealInteger', 'getMaximumDifference'));
		$nearbyMarkersServiceMock->method('transformIntoValidGeoCoordinate')->will($this->returnValue('0.123456'));
		$nearbyMarkersServiceMock->expects($this->exactly(2))->method('transformIntoValidGeoCoordinate');
		$nearbyMarkersServiceMock->_call('areGeoCordsNearEachOther', 12345, 12346);
	}

	/**
	 * @test
	 * @return void
	 */
	public function areGeoCordsNearEachOtherReturnsFalseIfTransformIntoValidGeoCoordinateThrowsException() {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('transformIntoValidGeoCoordinate', 'transformIntoRealInteger', 'getMaximumDifference'));
		$nearbyMarkersServiceMock->method('transformIntoValidGeoCoordinate')->will($this->throwException(new \Exception()));
		$this->assertFalse($nearbyMarkersServiceMock->_call('areGeoCordsNearEachOther', 12345, 12346));
	}

	/**
	 * @test
	 * @return void
	 */
	public function areGeoCordsNearEachOtherCallsGetMaximumDifference() {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('transformIntoValidGeoCoordinate', 'transformIntoRealInteger', 'getMaximumDifference'));
		$nearbyMarkersServiceMock->method('transformIntoValidGeoCoordinate')->will($this->returnValue('0.123456'));
		$nearbyMarkersServiceMock->expects($this->once())->method('getMaximumDifference')->will($this->returnValue(2000));
		$nearbyMarkersServiceMock->_call('areGeoCordsNearEachOther', 12345, 12346);
	}

	/**
	 * @test
	 * @return void
	 */
	public function transformIntoRealIntegerRemovesPeriodFromStringAndReturnsInteger() {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('foo'));
		$result = $nearbyMarkersServiceMock->_call('transformIntoRealInteger', '123.4567890');
		$this->assertEquals(1234567890, $result);
	}

	/**
	 * @param $value1
	 * @param $value2
	 * @param $maxDifference
	 * @param $expectation
	 *
	 * @dataProvider areGeoCordsNearEachOtherDataProvider
	 * @test
	 * @return void
	 */
	public function areGeoCordsNearEachOtherReturnsBooleanDependentOnDifferenceAndMaximumDifference($value1, $value2, $maxDifference, $expectation) {
		$nearbyMarkersServiceMock = $this->getAccessibleMock('\Lightwerk\Vectormap\Service\NearbyMarkersService', array('transformIntoValidGeoCoordinate', 'getMaximumDifference'));
		$nearbyMarkersServiceMock->method('transformIntoValidGeoCoordinate')->will($this->onConsecutiveCalls($value1, $value2));
		$nearbyMarkersServiceMock->method('getMaximumDifference')->will($this->returnValue($maxDifference));
		$this->assertEquals($expectation, $nearbyMarkersServiceMock->_call('areGeoCordsNearEachOther', $value1, $value2));
	}

	/**
	 * DataProvider for areGeoCordsNearEachOtherReturnsBooleanDependentOnDifferenceAndMaximumDifference
	 * @return array
	 */
	public function areGeoCordsNearEachOtherDataProvider() {
		return array(
			'valid 1' => array('1.111111', '1.111131', 60, TRUE),
			'valid 2' => array(123456, 123457, 2, TRUE),

			'invalid 1' => array(1000, 50, 1, FALSE),
		);
	}
}