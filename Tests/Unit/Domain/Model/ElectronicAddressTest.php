<?php

namespace Lightwerk\Vectormap\Tests\Unit\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Lightwerk\Vectormap\Domain\Model\ElectronicAddress;
use TYPO3\CMS\Core\Tests\BaseTestCase;

/**
 * Class ElectronicAddressTest
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class ElectronicAddressTest extends BaseTestCase {

	/**
	 * @dataProvider getModifiedIdentifierDataProvider
	 * @test
	 * @return void
	 */
	public function getModifiedIdentifierReturnsFullSchemedUrlForUrls($identifier, $expectation) {
		$electronicAddress = new ElectronicAddress();
		$electronicAddress->setIdentifier($identifier);
		$electronicAddress->setType('URL');
		$this->assertEquals($expectation, $electronicAddress->getModifiedIdentifier());
	}

	/**
	 * @return array
	 */
	public function getModifiedIdentifierDataProvider() {
		return array(
			'Adds scheme 1' => array('example.com', 'http://example.com'),
			'Adds scheme 2' => array('www.example.com', 'http://www.example.com'),
			'Keeps slash' => array('www.example.com/', 'http://www.example.com/'),
			'Keeps scheme https' => array('https://www.example.com/', 'https://www.example.com/'),
			'Keeps scheme ftp' => array('ftp://www.example.com/', 'ftp://www.example.com/'),
			'Keeps path' => array('www.example.com/my/path/', 'http://www.example.com/my/path/'),
			'Strips params' => array('www.example.com/my/path/?myParam=1', 'http://www.example.com/my/path/')
		);
	}

	/**
	 * @dataProvider isOfTypeDataProvider
	 * @test
	 * @return void
	 */
	public function isOfTypeReturnsBooleanDependentOnMatchingType($actualType, $checkedType, $expectation) {
		$electronicAddress = new ElectronicAddress();
		$electronicAddress->setType($actualType);
		$this->assertEquals($expectation, $electronicAddress->isOfType($checkedType));
	}

	/**
	 * @return array
	 */
	public function isOfTypeDataProvider() {
		return array(
			'Matching Set 1' => array('URL', 'URL', TRUE),
			'Matching Set 2' => array('url', 'URL', TRUE),
			'Matching Set 3' => array('URL', 'url', TRUE),
			'Matching Set 4' => array('uRl', 'UrL', TRUE),

			'Nonmatching Set 1' => array('URL', 'email', FALSE),
			'Nonmatching Set 2' => array('', 'URL', FALSE),
			'Nonmatching Set 3' => array('URL', '', FALSE),
		);
	}
}