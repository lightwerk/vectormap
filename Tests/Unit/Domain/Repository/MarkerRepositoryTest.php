<?php

namespace Lightwerk\Vectormap\Tests\Unit\Domain\Repository;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Tests\BaseTestCase;

/**
 * Class MarkerRepositoryTest
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class MarkerRepositoryTest extends BaseTestCase {

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\MarkerRepository
	 */
	protected $markerRepositoryMock = NULL;

	/**
	 * @return void
	 */
	protected function setUp() {
		$this->markerRepositoryMock = $this->getAccessibleMock(
			'Lightwerk\Vectormap\Domain\Repository\MarkerRepository',
			array('foo'),
			array($this->getMock('TYPO3\\CMS\\Extbase\\Object\\ObjectManagerInterface'))
		);
	}

	/**
	 * @return void
	 */
	public function tearDown() {
		$this->markerRepositoryMock = NULL;
	}

	/**
	 * @test
	 * @return void
	 */
	public function areCategoryConstraintsSetReturnTrueIfCategoriesAreEnabledAndNotEmpty() {
		$return = $this->markerRepositoryMock->_call('areCategoryConstraintsSet', array(
			'categories' => array(
				'enable' => '1',
				'list' => '1'
			)
		));
		$this->assertTrue($return);
	}

	/**
	 * @test
	 * @return void
	 */
	public function areCategoryConstraintsSetReturnFalseIfCategoriesAreNotEnabled() {
		$return = $this->markerRepositoryMock->_call('areCategoryConstraintsSet', array(
			'categories' => array(
				'enable' => '0',
				'list' => '1'
			)
		));
		$this->assertFalse($return);
	}

	/**
	 * @test
	 * @return void
	 */
	public function areCategoryConstraintsSetReturnFalseIfCategoriesAreEmpty() {
		$return = $this->markerRepositoryMock->_call('areCategoryConstraintsSet', array(
			'categories' => array(
				'enable' => '1',
				'list' => ''
			)
		));
		$this->assertFalse($return);
	}
}