<?php

namespace Lightwerk\Vectormap\Service;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\SingletonInterface;

/**
 * Configuration Service
 */
class ConfigurationService implements SingletonInterface {

	const STARTING_POINT_WORLD = 1;
	const STARTING_POINT_COUNTRY = 2;
	const STARTING_POINT_CONTINENTAL = 3;

	/**
	 * @var array
	 */
	public static $supportedContinentalRegions = array(
		'africa' => 'Africa',
		'asia' => 'Asia',
		'central-america' => 'Central America',
		'europe' => 'Europe',
		'northern-america' => 'Northern America',
		'oceania' => 'Oceania',
		'south-america' => 'South America'
	);

	/**
	 * modTSConfig per page
	 *
	 * @var array
	 */
	protected $modTSConfigPerPage;

	/**
	 * @var array
	 */
	protected $settings = array();

	/**
	 * @param array $settings
	 * @return array
	 */
	public function getSettings(array $settings) {
		if (empty($this->settings)) {
			$this->setSettings($settings);
		}
		return $this->settings;
	}

	/**
	 * @param array $settings
	 * @return void
	 */
	protected function setSettings(array $settings) {
		$settings = $this->getModifiedSettings($settings);
		$settings = $this->getTypeSecureSettings($settings);
		$this->settings = $settings;
	}

	/**
	 * Merges FlexForm settings into main settings
	 *
	 * @param array $settings
	 * @return array
	 */
	protected function getModifiedSettings(array $settings) {
		if (!empty($settings['flexform']) && is_array($settings['flexform'])) {
			$settings = $this->mergeFlexformSettings($settings);
		}
		if ((int)$settings['map']['jsOptions']['startingPoint'] === self::STARTING_POINT_COUNTRY) {
			$settings['countryFilter']['enable'] = 0;
		}
		if ((int)$settings['categories']['enable'] === 1 && !empty($settings['categories']['list']) && strpos($settings['categories']['list'], ',') === FALSE) {
			$settings['categoryFilter']['enable'] = 0;
		}
		if ((isset($settings['map']['enable']) && (int)$settings['map']['enable'] === 0) ||
			(isset($settings['map']['markers']['enable']) && (int)$settings['map']['markers']['enable'] === 0)
		) {
			$settings['map']['nearbyMarkers']['enable'] = 0;
		}
		if (isset($settings['map']['markers']['enable'])) {
			$settings['map']['jsOptions']['markers'] = (bool)$settings['map']['markers']['enable'];
		}
		if (isset($settings['categoryFilter']['enable'])) {
			$settings['map']['jsOptions']['categoryFilter'] = (bool)$settings['categoryFilter']['enable'];
		}
		if (isset($settings['countryFilter']['enable'])) {
			$settings['map']['jsOptions']['countryFilter'] = (bool)$settings['countryFilter']['enable'];
		}
		if (isset($settings['list']['hideOnStart'])) {
			$settings['map']['jsOptions']['hideListOnStart'] = (bool)$settings['list']['hideOnStart'];
		}
		return $settings;
	}

	/**
	 * @param array $settings
	 * @return array
	 */
	protected function mergeFlexformSettings(array $settings) {
		$settings = array_replace_recursive(
			$settings,
			$this->getArrayWithoutEmptyValues($settings['flexform'])
		);
		unset($settings['flexform']);
		return $settings;
	}

	/**
	 * Removes all empty values from array.
	 *
	 * @param array $array
	 * @return array
	 */
	protected function getArrayWithoutEmptyValues(array $array) {
		foreach ($array as $key => &$value) {
			if (is_array($value)) {
				$value = $this->getArrayWithoutEmptyValues($value);
				if ($value === array()) {
					unset($array[$key]);
				}
			} elseif ($value === '') {
				unset($array[$key]);
			}
		}
		return $array;
	}

	/**
	 * Converts all strings to proper types.
	 *
	 * @param array $settings
	 * @return array
	 */
	protected function getTypeSecureSettings(array $settings) {
		foreach ($settings as $key => $value) {
			if (is_array($value)) {
				$settings[$key] = $this->getTypeSecureSettings($value);
			} elseif (strtolower($value) === 'yes' || strtolower($value) === 'true') {
				$settings[$key] = TRUE;
			} elseif (strtolower($value) === 'no' || strtolower($value) === 'false') {
				$settings[$key] = FALSE;
			} elseif (preg_match('/^[0-9]+$/', $value)) {
				$settings[$key] = intval($value);
			} elseif (preg_match('/^[0-9]+\.[0-9]+$/', $value)) {
				$settings[$key] = floatval($value);
			}
		}
		return $settings;
	}

	/**
	 * Returns the modTSConfig for a page.
	 *
	 * @param $pageUid
	 * @return array
	 */
	public function getModTSConfig($pageUid) {
		if (empty($this->modTSConfigPerPage[$pageUid])) {
			$modTSConfig = BackendUtility::getModTSconfig($pageUid, 'tx_vectormap');
			$this->modTSConfigPerPage[$pageUid] = $modTSConfig['properties'];
		}
		return $this->modTSConfigPerPage[$pageUid];
	}
}