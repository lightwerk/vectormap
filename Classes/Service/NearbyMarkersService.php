<?php

namespace Lightwerk\Vectormap\Service;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\MathUtility;
use Lightwerk\Vectormap\Domain\Model\Marker;

/**
 * Class NearbyMarkerService
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class NearbyMarkersService implements SingletonInterface {

	const MAXIMUM_DIFFERENCE_DEFAULT = 5000;

	/**
	 * @var \Lightwerk\Vectormap\Service\ConfigurationService
	 * @inject
	 */
	protected $configurationService;

	/**
	 * @var integer
	 */
	protected $maximumDifference = 0;

	/**
	 * @param array $entries
	 * @return mixed
	 */
	public function enrichEntriesWithNearbyMarkers(array $entries) {
		foreach ($entries as $entry) {
			/** @var \Lightwerk\Vectormap\Domain\Model\Entry $entry */
			$markers = $entry->getMarkers();
			if ($markers->count() === 0) {
				continue;
			}
			$markers->rewind();
			foreach ($markers as $marker) {
				/** @var Marker $marker*/
				$marker->setNearbyMarkers($this->findNearbyMarkers($marker, $entries));
			}
		}
		return $entries;
	}

	/**
	 * @param Marker $initialMarker
	 * @param array $entries
	 * @return array
	 */
	protected function findNearbyMarkers(Marker $initialMarker, array $entries) {
		$nearbyMarkers = array();
		$lat = (string)$initialMarker->getLatitude();
		$lon = (string)$initialMarker->getLongitude();
		foreach ($entries as $entry) {
			/** @var \Lightwerk\Vectormap\Domain\Model\Entry $entry */
			$markers = $entry->getMarkers()->toArray();
			if (count($markers) === 0) {
				continue;
			}
			foreach ($markers as $marker) {
				/** @var Marker $marker*/
				if ($marker === $initialMarker) {
					continue;
				}
				if ($this->areGeoCordsNearEachOther($lat, $marker->getLatitude())) {
					if ($this->areGeoCordsNearEachOther($lon, $marker->getLongitude())) {
						if (!in_array($marker->getUid(), $nearbyMarkers)) {
							$nearbyMarkers[] = $marker->getUid();
						}
					}
				}
			}
		}
		return $nearbyMarkers;
	}

	/**
	 * @param string $geoPositionA
	 * @param string $geoPositionB
	 * @return boolean
	 */
	protected function areGeoCordsNearEachOther($geoPositionA, $geoPositionB) {
		try {
			$this->transformIntoValidGeoCoordinate($geoPositionA);
			$this->transformIntoValidGeoCoordinate($geoPositionB);
		} catch (\Exception $e) {
			return FALSE;
		}

		$geoPositionA = $this->transformIntoRealInteger($geoPositionA);
		$geoPositionB = $this->transformIntoRealInteger($geoPositionB);

		if ($geoPositionA >= $geoPositionB) {
			$diff = $geoPositionA - $geoPositionB;
		} else {
			$diff = $geoPositionB - $geoPositionA;
		}

		return $diff < $this->getMaximumDifference();
	}

	/**
	 * seventh position from the end of the string
	 * must be a period in a valid geo coordinate.
	 *
	 * like 9.123456
	 *
	 * Fills the string with zeros righthanded because those
	 * get not converted. So for 9.12 to return a valid coordinate
	 * we need to add a zero 4 times (9.120000).
	 *
	 * @param mixed $geoCoordinate
	 * @return boolean
	 * @throws \Exception
	 */
	protected function transformIntoValidGeoCoordinate($geoCoordinate) {
		$geoCoordinate = (string)$geoCoordinate;
		if (!MathUtility::canBeInterpretedAsFloat($geoCoordinate)) {
			throw new \Exception('Geo coordinate must bei interpretable as integer.', 1429538913);
		}
		if (strpos($geoCoordinate, '.') === FALSE) {
			throw new \Exception('Passed string' . $geoCoordinate . ' could not be converted into a valid geo coordinate due to missing period.', 1429538914);
		}
		for ($i = 0; $i < 6; $i ++) {
			if (substr($geoCoordinate, -7, 1) === '.') {
				return $geoCoordinate;
			} else {
				$geoCoordinate = $geoCoordinate . '0';
			}
		}
		throw new \Exception('Passed string' . $geoCoordinate . ' could not be converted into a valid geo coordinate.', 1429538915);
	}

	/**
	 * 9.123456 => 9123456
	 *
	 * @param mixed $value
	 * @return integer
	 */
	protected function transformIntoRealInteger($value) {
		return (int)str_replace('.', '', (string)$value);
	}

	/**
	 * @return integer
	 */
	protected function getMaximumDifference() {
		if ($this->maximumDifference === 0) {
			$settings = $this->configurationService->getSettings(array());
			if (isset($settings['map']) && isset($settings['map']['nearbyMarkers']) && isset($settings['map']['nearbyMarkers']['maximumDifference'])) {
				$this->maximumDifference = (int)$settings['map']['nearbyMarkers']['maximumDifference'];
			} else {
				$this->maximumDifference = self::MAXIMUM_DIFFERENCE_DEFAULT;
			}
		}
		return $this->maximumDifference;
	}
}