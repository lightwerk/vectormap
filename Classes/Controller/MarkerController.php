<?php

namespace Lightwerk\Vectormap\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Marker Controller
 */
class MarkerController extends ActionController {

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\EntryRepository
	 * @inject
	 */
	protected $entryRepository = NULL;

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository = NULL;

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\CountryRepository
	 * @inject
	 */
	protected $countryRepository = NULL;

	/**
	 * @var \Lightwerk\Vectormap\Service\ConfigurationService
	 * @inject
	 */
	protected $configurationService = NULL;

	/**
	 * @var \Lightwerk\Vectormap\Service\NearbyMarkersService
	 * @inject
	 */
	protected $nearbyMarkersService = NULL;

	/**
	 * @return void
	 */
	public function initializeObject() {
		$this->settings = $this->configurationService->getSettings($this->settings);
	}

	/**
	 * Index Action
	 *
	 * @return void
	 */
	public function indexAction() {
		$entries = $this->entryRepository->findBySettings($this->settings);
		if (isset($this->settings['map']['nearbyMarkers']['enable']) && (int)$this->settings['map']['nearbyMarkers']['enable'] === 1) {
			$entries = $this->nearbyMarkersService->enrichEntriesWithNearbyMarkers($entries);
		}
		$this->view
			->assign('entries', $entries)
			->assign('categories', $this->categoryRepository->findAllByEntries($entries, $this->settings))
			->assign('countries', $this->countryRepository->findAllByEntriesAndSettings($entries, $this->settings));
	}

	/**
	 * Select Action
	 *
	 * @return void
	 */
	public function selectAction() {
		$entries = $this->entryRepository->findAll();
		$this->view
			->assign('entries', $entries)
			->assign('categories', $this->categoryRepository->findAllByEntries($entries))
			->assign('countries', $this->countryRepository->findAllByEntries($entries));
	}
}