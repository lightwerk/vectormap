<?php

namespace Lightwerk\Vectormap\Domain\Model;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * Class Entry
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class Entry extends AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * street
	 *
	 * @var string
	 */
	protected $street = '';

	/**
	 * houseNumber
	 *
	 * @var string
	 */
	protected $houseNumber = '';

	/**
	 * zipCode
	 *
	 * @var string
	 */
	protected $zipCode = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $city = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $position = '';

	/**
	 * electronicAddresses
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\ElectronicAddress>
	 * @cascade remove
	 */
	protected $electronicAddresses = NULL;

	/**
	 * markers
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\Marker>
	 * @cascade remove
	 */
	protected $markers = NULL;

	/**
	 * markers
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\Region>
	 * @cascade remove
	 */
	protected $regions = NULL;

	/**
	 * salutation
	 *
	 * @var integer
	 */
	protected $salutation = 0;

	/**
	 * information
	 *
	 * @var string
	 */
	protected $information = '';

	/**
	 * images
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 */
	protected $images = NULL;

	/**
	 * categories
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Lightwerk\Vectormap\Domain\Model\Category>
	 */
	protected $categories = NULL;

	/**
	 * country
	 *
	 * @validate NotEmpty
	 * @var \Lightwerk\Vectormap\Domain\Model\Country
	 */
	protected $country = NULL;

	/**
	 * @var array
	 */
	protected $markerUids = array();

	/**
	 * @var array
	 */
	protected $markerCountryCodes = array();

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->images = new ObjectStorage();
		$this->categories = new ObjectStorage();
		$this->electronicAddresses = new ObjectStorage();
		$this->markers = new ObjectStorage();
		$this->regions = new ObjectStorage();
	}

	/**
	 * Adds a ElectronicAddress
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\ElectronicAddress $electronicAddress
	 * @return void
	 */
	public function addElectronicAddress(ElectronicAddress $electronicAddress) {
		$this->electronicAddresses->attach($electronicAddress);
	}

	/**
	 * Removes a ElectronicAddress
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\ElectronicAddress $electronicAddressToRemove The ElectronicAddress to be removed
	 * @return void
	 */
	public function removeElectronicAddress(ElectronicAddress $electronicAddressToRemove) {
		$this->electronicAddresses->detach($electronicAddressToRemove);
	}

	/**
	 * Returns the electronicAddresses
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\ElectronicAddress> $electronicAddresses
	 */
	public function getElectronicAddresses() {
		return $this->electronicAddresses;
	}

	/**
	 * Sets the electronicAddresses
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\ElectronicAddress> $electronicAddresses
	 * @return void
	 */
	public function setElectronicAddresses(ObjectStorage $electronicAddresses) {
		$this->electronicAddresses = $electronicAddresses;
	}

	/**
	 * Returns the salutation
	 *
	 * @return integer $salutation
	 */
	public function getSalutation() {
		return $this->salutation;
	}

	/**
	 * Sets the salutation
	 *
	 * @param integer $salutation
	 * @return void
	 */
	public function setSalutation($salutation) {
		$this->salutation = $salutation;
	}

	/**
	 * Returns the information
	 *
	 * @return string $information
	 */
	public function getInformation() {
		return $this->information;
	}

	/**
	 * Returns the information with correct img sources
	 *
	 * @return string $informationForJson
	 */
	public function getInformationForJson() {
		$informationForJson = preg_replace('/src="fileadmin\/_processed/', 'src="/fileadmin/_processed', $this->information);
		$informationForJson = str_replace('"', "'", $informationForJson);
		return nl2br(preg_replace('/src="uploads\//', 'src="/uploads/', $informationForJson));
	}

	/**
	 * Sets the information
	 *
	 * @param string $information
	 * @return void
	 */
	public function setInformation($information) {
		$this->information = $information;
	}

	/**
	 * Adds a Image
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
	 * @return void
	 */
	public function addImage(FileReference $image) {
		$this->images->attach($image);
	}

	/**
	 * Removes a Image
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The Image to be removed
	 * @return void
	 */
	public function removeImage(FileReference $imageToRemove) {
		$this->images->detach($imageToRemove);
	}

	/**
	 * Returns the images
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<FileReference> $images
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * Adds a Category
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(Category $category) {
		$this->categories->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\Category $categoryToRemove The Category to be removed
	 * @return void
	 */
	public function removeCategory(Category $categoryToRemove) {
		$this->categories->detach($categoryToRemove);
	}

	/**
	 * Returns the categories
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\Category> $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\Category> $categories
	 * @return void
	 */
	public function setCategories(ObjectStorage $categories) {
		$this->categories = $categories;
	}

	/**
	 * @return array
	 */
	public function getCategoryUids() {
		$categories = $this->getCategories();
		$categoryUids = array();
		foreach ($categories as $category) {
			/** @var Category $category */
			$categoryUids = array_merge($categoryUids, $category->getRootLine());
		}
		$categoryUids = array_unique($categoryUids);
		return $categoryUids;
	}

	/**
	 * Adds a Marker
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\Marker $marker
	 * @return void
	 */
	public function addMarker(Marker $marker) {
		$this->markers->attach($marker);
	}

	/**
	 * Removes a Marker
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\Marker $markerToRemove The Marker to be removed
	 * @return void
	 */
	public function removeMarker(Marker $markerToRemove) {
		$this->markers->detach($markerToRemove);
	}

	/**
	 * Returns the markers
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\Marker> $markers
	 */
	public function getMarkers() {
		return $this->markers;
	}

	/**
	 * Sets the markers
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Lightwerk\Vectormap\Domain\Model\Marker> $markers
	 * @return void
	 */
	public function setMarkers(ObjectStorage $markers) {
		$this->markers = $markers;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * @param string $street
	 * @return void
	 */
	public function setStreet($street) {
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getHouseNumber() {
		return $this->houseNumber;
	}

	/**
	 * @param string $houseNumber
	 * @return void
	 */
	public function setHouseNumber($houseNumber) {
		$this->houseNumber = $houseNumber;
	}

	/**
	 * @return string
	 */
	public function getZipCode() {
		return $this->zipCode;
	}

	/**
	 * @param string $zipCode
	 * @return void
	 */
	public function setZipCode($zipCode) {
		$this->zipCode = $zipCode;
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the country
	 *
	 * @return Country $country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Sets the country
	 *
	 * @param Country $country
	 * @return void
	 */
	public function setCountry(Country $country = NULL) {
		$this->country = $country;
	}

	/**
	 * @return ObjectStorage
	 */
	public function getRegions() {
		return $this->regions;
	}

	/**
	 * @param ObjectStorage $regions
	 * @return void
	 */
	public function setRegions(ObjectStorage $regions) {
		$this->regions = $regions;
	}

	/**
	 * Adds a Region
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\Region $region
	 * @return void
	 */
	public function addRegion(Region $region) {
		$this->regions->attach($region);
	}

	/**
	 * @return string
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * @param string $position
	 * @return void
	 */
	public function setPosition($position) {
		$this->position = $position;
	}

	/**
	 * Removes a Region
	 *
	 * @param \Lightwerk\Vectormap\Domain\Model\Region $regionToRemove The Region to be removed
	 * @return void
	 */
	public function removeRegion(Region $regionToRemove) {
		$this->regions->detach($regionToRemove);
	}

	/**
	 * @return void
	 */
	protected function prepareMarkerInformations() {
		/** @var Marker $marker */
		foreach ($this->markers as $marker) {
			$this->markerUids[] = $marker->getUid();
			if (!in_array($marker->getCountry()->getIsoCodeA2(), $this->markerCountryCodes)) {
				$this->markerCountryCodes[] = $marker->getCountry()->getIsoCodeA2();
			}
		}
	}

	/**
	 * @return array
	 */
	public function getRegionsForJson() {
		$regions = array();
		foreach ($this->regions as $region) {
			/** @var Region $region */
			$regions[] = $region->getCountry()->getIsoCodeA2();
		}
		return $regions;
	}

	/**
	 * @return array
	 */
	public function getRegionCodes() {
		$regionCodes = array();
		foreach ($this->markers as $marker) {
			/** @var Marker $marker */
			$regionCodes[] = $marker->getCountry()->getIsoCodeA2();
		}
		foreach ($this->regions as $region) {
			/** @var Region $region */
			$regionCodes[] = $region->getCountry()->getIsoCodeA2();
		}
		return array_unique($regionCodes);
	}

	/**
	 * Concatenated strings that are searched in frontend
	 * to reduce overhead in JS app.
	 *
	 * @return string
	 */
	public function getSearchString() {
		$searchStringParts = array();
		$searchStringParts[] = $this->getName();
		$searchStringParts[] = $this->getCity();
		$searchStringParts[] = $this->getCountry()->getName();
		$searchStringParts[] = $this->getStreet();
		foreach ($this->getMarkers() as $marker) {
			/** @var Marker $marker */
			$searchStringParts[] = $marker->getSearchString();
		}
		$searchStringParts = array_unique($searchStringParts);
		return implode(' ', $searchStringParts);
	}

	/**
	 * @return array
	 */
	public function __toJson() {
		$this->prepareMarkerInformations();
		$converted = ObjectAccess::getGettableProperties($this);
		$converted['countryIsoCodeA2'] = $this->getCountry()->getIsoCodeA2();
		$converted['countryName'] = htmlentities($this->getCountry()->getName(), ENT_QUOTES);
		$converted['information'] = htmlentities($this->getInformationForJson(), ENT_QUOTES);
		$converted['markerUids'] = $this->markerUids;
		$converted['markerCountryCodes'] = $this->markerCountryCodes;

		$converted['city'] = htmlentities($converted['city'], ENT_QUOTES);
		$converted['street'] = htmlentities($converted['street'], ENT_QUOTES);
		$converted['position'] = htmlentities($converted['position'], ENT_QUOTES);
		$converted['name'] = htmlentities($converted['name'], ENT_QUOTES);
		$converted['searchString'] = htmlentities($converted['searchString'], ENT_QUOTES);
		$converted['regions'] = $this->getRegionsForJson();

		// remove properties we dont need in JSON
		unset($converted['country']);
		unset($converted['pid']);
		unset($converted['regionsForJson']);
		unset($converted['categories']);
		unset($converted['images']);
		unset($converted['informationForJson']);
		return $converted;
	}
}