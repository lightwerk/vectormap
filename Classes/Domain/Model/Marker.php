<?php

namespace Lightwerk\Vectormap\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * Marker
 */
class Marker extends AbstractEntity {

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * street
	 *
	 * @var string
	 */
	protected $street = '';

	/**
	 * houseNumber
	 *
	 * @var string
	 */
	protected $houseNumber = '';

	/**
	 * zipCode
	 *
	 * @var string
	 */
	protected $zipCode = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $city = '';

	/**
	 * This property holds markers that are geographically nearby.
	 * It is filled in the MarkerRepository and uses to render multiple
	 * marker tooltips as one big tooltip if markers are so close to each
	 * other that they appear as one single point on the map.
	 *
	 * @var array
	 */
	protected $nearbyMarkers = array();

	/**
	 * country
	 *
	 * @validate NotEmpty
	 * @var \Lightwerk\Vectormap\Domain\Model\Country
	 */
	protected $country = NULL;

	/**
	 * latitude
	 *
	 * @var float
	 */
	protected $latitude = 0.0;

	/**
	 * longitude
	 *
	 * @var float
	 */
	protected $longitude = 0.0;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->categories = new ObjectStorage();
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the street
	 *
	 * @return string $street
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * Sets the street
	 *
	 * @param string $street
	 * @return void
	 */
	public function setStreet($street) {
		$this->street = $street;
	}

	/**
	 * Returns the houseNumber
	 *
	 * @return string $houseNumber
	 */
	public function getHouseNumber() {
		return $this->houseNumber;
	}

	/**
	 * Sets the houseNumber
	 *
	 * @param string $houseNumber
	 * @return void
	 */
	public function setHouseNumber($houseNumber) {
		$this->houseNumber = $houseNumber;
	}

	/**
	 * Returns the zipCode
	 *
	 * @return string $zipCode
	 */
	public function getZipCode() {
		return $this->zipCode;
	}

	/**
	 * Sets the zipCode
	 *
	 * @param string $zipCode
	 * @return void
	 */
	public function setZipCode($zipCode) {
		$this->zipCode = $zipCode;
	}

	/**
	 * Returns the city
	 *
	 * @return string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the country
	 *
	 * @return Country $country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Sets the country
	 *
	 * @param Country $country
	 * @return void
	 */
	public function setCountry(Country $country) {
		$this->country = $country;
	}

	/**
	 * Returns the latitude
	 *
	 * @return float $latitude
	 */
	public function getLatitude() {
		return $this->latitude;
	}

	/**
	 * Sets the latitude
	 *
	 * @param float $latitude
	 * @return void
	 */
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}

	/**
	 * Returns the longitude
	 *
	 * @return float $longitude
	 */
	public function getLongitude() {
		return $this->longitude;
	}

	/**
	 * Sets the longitude
	 *
	 * @param float $longitude
	 * @return void
	 */
	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}

	/**
	 * @return array
	 */
	public function getNearbyMarkers() {
		return $this->nearbyMarkers;
	}

	/**
	 * @param array $nearbyMarkers
	 * @return void
	 */
	public function setNearbyMarkers($nearbyMarkers) {
		$this->nearbyMarkers = $nearbyMarkers;
	}

	/**
	 * @return string
	 */
	public function getSearchString() {
		$searchStringParts = array();
		$searchStringParts[] = $this->getName();
		$searchStringParts[] = $this->getCity();
		$searchStringParts[] = $this->getCountry()->getName();
		$searchStringParts[] = $this->getStreet();
		return implode(' ', $searchStringParts);
	}

	/**
	 * @return array
	 */
	public function __toJson() {
		$converted = ObjectAccess::getGettableProperties($this);
		$converted['countryIsoCodeA2'] = $this->getCountry()->getIsoCodeA2();
		$converted['countryName'] = htmlentities($this->getCountry()->getName(), ENT_QUOTES);
		$converted['name'] = htmlentities($converted['name'], ENT_QUOTES);
		$converted['street'] = htmlentities($converted['street'], ENT_QUOTES);
		$converted['city'] = htmlentities($converted['city'], ENT_QUOTES);
		$converted['searchString'] = htmlentities($converted['searchString'], ENT_QUOTES);

		unset($converted['pid']);
		unset($converted['searchString']);
		unset($converted['country']);

		return $converted;
	}
}