<?php

namespace Lightwerk\Vectormap\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * ElectronicAddress
 */
class ElectronicAddress extends AbstractEntity {

	/**
	 * type
	 *
	 * @var string
	 */
	protected $type = '';

	/**
	 * identifier
	 *
	 * @var string
	 */
	protected $identifier = '';

	/**
	 * comment
	 *
	 * @var string
	 */
	protected $comment = '';

	/**
	 * Returns the type
	 *
	 * @return string $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Sets the type
	 *
	 * @param string $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * Returns the identifier
	 *
	 * @return string $identifier
	 */
	public function getIdentifier() {
		return $this->identifier;
	}

	/**
	 * Ensures that a URL is prepended with a protocol
	 *
	 * @return string
	 */
	public function getModifiedIdentifier() {
		if ($this->isOfType('url')) {
			$urlArray = parse_url($this->identifier);
			$urlArray['host'] = $urlArray['host'] ?: '';
			$urlArray['scheme'] = $urlArray['scheme'] ?: 'http';
			return $urlArray['scheme'] . '://' .  $urlArray['host'] . $urlArray['path'];
		}
		return $this->identifier;
	}

	/**
	 * Sets the identifier
	 *
	 * @param string $identifier
	 * @return void
	 */
	public function setIdentifier($identifier) {
		$this->identifier = $identifier;
	}

	/**
	 * Returns the comment
	 *
	 * @return string $comment
	 */
	public function getComment() {
		return $this->comment;
	}

	/**
	 * Sets the comment
	 *
	 * @param string $comment
	 * @return void
	 */
	public function setComment($comment) {
		$this->comment = $comment;
	}

	/**
	 * @param string $type
	 * @return boolean
	 */
	public function isOfType($type = '') {
		return strtolower($this->type) === strtolower($type);
	}

	/**
	 * @return array
	 */
	public function __toJson() {
		$converted = ObjectAccess::getGettableProperties($this);
		$converted['isEmail'] = $this->isOfType('email');
		$converted['isUrl'] = $this->isOfType('url');
		$converted['modifiedIdentifier'] = $this->getModifiedIdentifier();
		if (!$converted['isUrl'] && $converted['modifiedIdentifier'] === $converted['identifier']) {
			unset($converted['modifiedIdentifier']);
		}
		$converted['comment'] = htmlentities($converted['comment'], ENT_QUOTES);
		unset($converted['pid']);
		unset($converted['uid']);
		unset($converted['ofType']);
		return $converted;
	}

}