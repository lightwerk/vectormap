<?php
namespace Lightwerk\Vectormap\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Lightwerk\Vectormap\Domain\Model\Entry;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;

/**
 * The repository for categories
 */
class CategoryRepository implements SingletonInterface {

	/**
	 * Returns all categories by markers.
	 *
	 * @param Entry[] $entries
	 * @param array $settings
	 * @return Category[]
	 */
	public function findAllByEntries($entries, $settings = array()) {
		$categories = array();
		$allowedCategories = $this->getAllowedCategoriesFromSettings($settings);
		$categoryConstrains = !empty($allowedCategories);
		foreach ($entries as $entry) {
			/** @var Category[] $entryCategories */
			$entryCategories = $entry->getCategories();
			foreach ($entryCategories as $category) {
				if (!$categoryConstrains) {
					$categories[$category->getUid()] = $category;
					continue;
				}
				if ($categoryConstrains && in_array($category->getUid(), $allowedCategories)) {
					$categories[$category->getUid()] = $category;
				}
			}
		}
		return $this->sortCategoriesByTitle($categories);
	}

	/**
	 * @param array $settings
	 * @return array
	 */
	protected function getAllowedCategoriesFromSettings(array $settings) {
		if (empty($settings)) {
			return array();
		}
		if (isset($settings['categories']) &&
			isset($settings['categories']['enable']) &&
			(int)$settings['categories']['enable'] === 1 &&
			isset($settings['categories']['list'])
		) {
			return GeneralUtility::intExplode(',', $settings['categories']['list']);
		}
		return array();
	}

	/**
	 * Sorts an Array of Category objects
	 * alphabetically by their titles.
	 *
	 * @param Category[] $categories
	 * @return Category[]
	 */
	public function sortCategoriesByTitle($categories) {
		// suppress warnings due to https://bugs.php.net/bug.php?id=50688
		@usort($categories, function($a, $b) {
			/** @var Category $a */
			/** @var Category $b */
			return strcmp(strtolower($a->getTitle()), strtolower($b->getTitle()));
		});
		return $categories;
	}
}