<?php

namespace Lightwerk\Vectormap\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Lightwerk\Vectormap\Service\ConfigurationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;
use SJBR\StaticInfoTables\Domain\Model\Country;

/**
 * The repository for Markers
 */
class MarkerRepository extends Repository {

	/**
	 * @var \SJBR\StaticInfoTables\Domain\Repository\CountryRepository
	 * @inject
	 */
	protected $staticCountryRepository;

	/**
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findAllInDatabase() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface */
		$querySettings = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface');
		$querySettings->setRespectStoragePage(FALSE);
		$this->setDefaultQuerySettings($querySettings);
		return $this->findAll();
	}

	/**
	 * @param $settings
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 * @throws \Exception
	 */
	public function findBySettings($settings) {
		$hasCategoryConstraints = $this->areCategoryConstraintsSet($settings);
		switch ((int)$settings['map']['jsOptions']['startingPoint']) {
			// world
			case ConfigurationService::STARTING_POINT_WORLD:
				if ($hasCategoryConstraints) {
					return $this->findByCategories($settings['categories']['list']);
				}
				return $this->findAll();
			// country
			case ConfigurationService::STARTING_POINT_COUNTRY:
				$country = $this->staticCountryRepository->findOneByIsoCodeA2(strtoupper($settings['map']['jsOptions']['country']));
				if ($hasCategoryConstraints) {
					return $this->findByCategoriesAndCountry($settings['categories']['list'], $country);
				}
				return $this->findByCountry($country);
			default:
				throw new \Exception('Unknown starting Point configured. Check Plugin Options.', 1425332250);
		}
	}

	/**
	 * @param string $categories
	 * @param Country $country
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByCategoriesAndCountry($categories, Country $country) {
		$query = $this->createQuery();
		$andConstraints = array(
			$query->equals('country', $country)
		);
		if (strpos($categories, ',') === FALSE) {
			$andConstraints[] = $query->contains('categories', $categories);
		} else {
			$andConstraints[] = $query->logicalOr($this->getCategoryConstraints($categories));
		}
		$query->matching(
			$query->logicalAnd(
				$andConstraints
			)
		);
		return $query->execute();
	}

	/**
	 * @param string $categories
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByCategories($categories) {
		$query = $this->createQuery();
		if (strpos($categories, ',') === FALSE) {
			$query->matching($query->contains('categories', $categories));
		} else {
			$query->matching(
				$query->logicalOr(
					$this->getCategoryConstraints($categories)
				)
			);
		}
		return $query->execute();
	}

	/**
	 * @param $categories
	 * @return array
	 */
	protected function getCategoryConstraints($categories) {
		$query = $this->createQuery();
		$categoryArray = GeneralUtility::intExplode(',', $categories);
		$categoryConstraints = array();
		foreach ($categoryArray as $category) {
			$categoryConstraints[] = $query->contains('categories', $category);
		}
		return $categoryConstraints;
	}

	/**
	 * @param array $settings
	 * @return bool
	 */
	protected function areCategoryConstraintsSet(array $settings) {
		return (int)$settings['categories']['enable'] === 1 && !empty($settings['categories']['list']);
	}
}