<?php

namespace Lightwerk\Vectormap\Domain\Repository;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\SingletonInterface;
use Lightwerk\Vectormap\Service\ConfigurationService;

/**
 * Class StaticInfoTablesRepository
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class StaticInfoTablesRepository implements SingletonInterface {

	/**
	 * @var \SJBR\StaticInfoTables\Domain\Repository\CountryRepository
	 * @inject
	 */
	protected $staticCountryRepository;

	/**
	 * @var \SJBR\StaticInfoTables\Domain\Repository\TerritoryRepository
	 * @inject
	 */
	protected $staticTerritoryRepository;

	/**
	 * @param string $isoCode - e.g. "DE" or "ENG"
	 * @return \SJBR\StaticInfoTables\Domain\Model\Country | NULL
	 * @throws \Exception
	 */
	public function findCountryByIsoCode($isoCode) {
		if (strlen($isoCode) === 2) {
			return $this->staticCountryRepository->findOneByIsoCodeA2(strtoupper($isoCode));
		}
		if (strlen($isoCode) === 3) {
			return $this->staticCountryRepository->findOneByIsoCodeA3(strtoupper($isoCode));
		}
		throw new \Exception('Iso code mus have either 2 or 3 characters. ' . $isoCode . ' given.', 1427968570);
	}

	/**
	 * @param string $territoryName
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 * @throws \Exception
	 */
	public function findCountriesByTerritoryName($territoryName) {
		if (!isset(ConfigurationService::$supportedContinentalRegions[$territoryName])) {
			throw new \Exception('Unknown region name: ' . $territoryName, 1427968569);
		}
		$territory = $this->staticTerritoryRepository->findOneByNameEn(ConfigurationService::$supportedContinentalRegions[$territoryName]);
		$countries = $this->staticCountryRepository->findByTerritory($territory);
		return $countries;
	}

	/**
	 * @param string $territoryName
	 * @return array
	 * @throws \Exception
	 */
	public function getIdArrayOfCountriesInTerritory($territoryName) {
		$countries = $this->findCountriesByTerritoryName($territoryName);
		$idArray = array();
		foreach ($countries as $country) {
			$idArray[] = $country->getUid();
		}
		return $idArray;
	}
}