<?php

namespace Lightwerk\Vectormap\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Lightwerk\Vectormap\Domain\Model\Entry;
use Lightwerk\Vectormap\Domain\Model\Country;
use TYPO3\CMS\Core\SingletonInterface;
use Lightwerk\Vectormap\Service\ConfigurationService;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * The repository for countries
 */
class CountryRepository implements SingletonInterface {

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\StaticInfoTablesRepository
	 * @inject
	 */
	protected $staticInfoTablesRepository;

	/**
	 * @param Entry[] $entries
	 * @param array $settings
	 * @return Country[]
	 * @throws \Exception
	 */
	public function findAllByEntriesAndSettings($entries, $settings = array()) {
		if (empty($settings)) {
			return $this->findAllByEntries($entries);
		}
		if (isset($settings['map']['jsOptions']['startingPoint'])) {
			switch ((int)$settings['map']['jsOptions']['startingPoint']) {
				case ConfigurationService::STARTING_POINT_WORLD:
					return $this->findAllByEntries($entries);

				case ConfigurationService::STARTING_POINT_CONTINENTAL:
					$countryWhiteList = $this->staticInfoTablesRepository->getIdArrayOfCountriesInTerritory($settings['map']['jsOptions']['continental']);
					return $this->findAllByEntries($entries, $countryWhiteList);

				case ConfigurationService::STARTING_POINT_COUNTRY:
					$country = $this->staticInfoTablesRepository->findCountryByIsoCode($settings['map']['jsOptions']['country']);
					return array($country);
				default:
					throw new \Exception('Unknown starting Point configured. Check Plugin Options.', 1425332251);
			}
		}
	}

	/**
	 * Returns all categories by $entries.
	 *
	 * @param Entry[] $entries
	 * @param array $countryWhiteList
	 * @return Country[]
	 */
	public function findAllByEntries($entries, $countryWhiteList = array()) {
		$countries = array();
		foreach ($entries as $entry) {
			$country = $entry->getCountry();
			if ($country instanceof Country) {
				$countries[$country->getUid()] = $country;
			}

			foreach ($entry->getMarkers() as $marker) {
				$country = $marker->getCountry();
				if ($country instanceof Country) {
					$countries[$country->getUid()] = $country;
				}
			}

			// We reattach the regions here because in case we just show a single
			// continent we don't want regions outside that continent marked on the
			// map (we have a $countryWhiteList then).
			// This is not the ideal solution, but seals the deal for now.
			$regions = new ObjectStorage();
			foreach ($entry->getRegions() as $region) {
				$country = $region->getCountry();
				if ($country instanceof Country) {
					if (empty($countryWhiteList) || in_array($country->getUid(), $countryWhiteList)) {
						$countries[$country->getUid()] = $country;
						$regions->attach($region);
					}
				}
			}
			$entry->setRegions($regions);
		}
		if (!empty($countries)) {
			return $this->sortCountriesByName($countries);
		}
		return $countries;
	}

	/**
	 * Sorts an Array of Country objects
	 * alphabetically by their localized Name.
	 *
	 * @param Country[] $countries
	 * @return Country[]
	 */
	public function sortCountriesByName($countries) {
		// suppress warnings due to https://bugs.php.net/bug.php?id=50688
		@usort($countries, function($a, $b) {
			/** @var Country $a */
			/** @var Country $b */
			return strcmp(strtolower($a->getName()), strtolower($b->getName()));
		});
		return $countries;
	}
}