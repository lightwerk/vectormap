<?php

namespace Lightwerk\Vectormap\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Daniel Goerz <dlg@lightwerk.com>, Lightwerk GmbH
 *           Lars Malach <lm@lightwerk.com>, Lightwerk GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Lightwerk\Vectormap\Service\ConfigurationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Markers
 */
class EntryRepository extends Repository {

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\StaticInfoTablesRepository
	 * @inject
	 */
	protected $staticInfoTablesRepository;

	/**
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findAllInDatabase() {
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface */
		$querySettings = $this->objectManager->get('TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface');
		$querySettings->setRespectStoragePage(FALSE);
		$this->setDefaultQuerySettings($querySettings);
		return $this->findAll();
	}

	/**
	 * @param $settings
	 * @return array
	 * @throws \Exception
	 */
	public function findBySettings($settings) {
		$hasCategoryConstraints = $this->areCategoryConstraintsSet($settings);
		switch ((int)$settings['map']['jsOptions']['startingPoint']) {
			// world
			case ConfigurationService::STARTING_POINT_WORLD:
				if ($hasCategoryConstraints) {
					return $this->findByCategories($settings['categories']['list']);
				}
				return $this->findAll()->toArray();
			// country
			case ConfigurationService::STARTING_POINT_COUNTRY:
				$country = $this->staticInfoTablesRepository->findCountryByIsoCode($settings['map']['jsOptions']['country']);
				if ($hasCategoryConstraints) {
					return $this->findByCategoriesAndCountries($settings['categories']['list'], array($country));
				}
				return $this->findByCountries(array($country));
			// continental
			case ConfigurationService::STARTING_POINT_CONTINENTAL:
				$countries = $this->staticInfoTablesRepository->findCountriesByTerritoryName($settings['map']['jsOptions']['continental']);
				if ($hasCategoryConstraints) {
					return $this->findByCategoriesAndCountries($settings['categories']['list'], $countries);
				}
				return $this->findByCountries($countries);
			default:
				throw new \Exception('Unknown starting Point configured. Check Plugin Options.', 1425332250);
		}
	}

	/**
	 * @param string $categories
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByCategories($categories) {
		$query = $this->createQuery();
		$query->matching($this->getCategoryConstraints($categories));
		return $query->execute()->toArray();
	}

	/**
	 * @param string $categories
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface|array $countries
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByCategoriesAndCountries($categories, $countries) {
		$entries = $this->findByInCountriesAndCategoriesConstraints($countries, $categories, 'country')->toArray();
		$entries = array_merge($entries, $this->findByInCountriesAndCategoriesConstraints($countries, $categories, 'markers.country')->toArray());
		$entries = array_merge($entries, $this->findByInCountriesAndCategoriesConstraints($countries, $categories, 'regions.country')->toArray());
		return $this->makeUnique($entries);
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface|array $countries
	 * @return array
	 */
	public function findByCountries($countries) {
		$entries = $this->findByInCountriesConstraint($countries, 'country')->toArray();
		$entries = array_merge($entries, $this->findByInCountriesConstraint($countries, 'markers.country')->toArray());
		$entries = array_merge($entries, $this->findByInCountriesConstraint($countries, 'regions.country')->toArray());
		return $this->makeUnique($entries);
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface $countries
	 * @param string $field
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByInCountriesConstraint($countries, $field) {
		$query = $this->createQuery();
		return $query->matching($query->in($field, $countries))->execute();
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\QueryResultInterface $countries
	 * @param string $categories
	 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByInCountriesAndCategoriesConstraints($countries, $categories, $field) {
		$query = $this->createQuery();
		return $query->matching(
			$query->logicalAnd(
				$this->getCategoryConstraints($categories),
				$query->in($field, $countries)
			)
		)->execute();
	}

	/**
	 * @param string $categories
	 * @return object
	 */
	protected function getCategoryConstraints($categories) {
		$query = $this->createQuery();
		if (strpos($categories, ',') === FALSE) {
			return $query->contains('categories', $categories);
		}
		$categoryArray = GeneralUtility::intExplode(',', $categories);
		$categoryConstraints = array();
		foreach ($categoryArray as $category) {
			$categoryConstraints[] = $query->contains('categories', $category);
		}
		return $query->logicalOr($categoryConstraints);
	}

	/**
	 * @param array $settings
	 * @return bool
	 */
	protected function areCategoryConstraintsSet(array $settings) {
		return (int)$settings['categories']['enable'] === 1 && !empty($settings['categories']['list']);
	}

	/**
	 * @param array $entries
	 * @return array
	 */
	protected function makeUnique(array $entries) {
		$uidStorage = array();
		$uniqueEntries = array();
		foreach ($entries as $entry) {
			/* @var \Lightwerk\Vectormap\Domain\Model\Entry $entry */
			if (!isset($uidStorage[(string)$entry->getUid()])) {
				$uidStorage[(string)$entry->getUid()] = $entry->getUid();
				$uniqueEntries[] = $entry;
			}
		}
		return $uniqueEntries;
	}
}