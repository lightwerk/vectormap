<?php

namespace Lightwerk\Vectormap\UserFunction\FlexForm;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Lightwerk\Vectormap\Service\ConfigurationService;

/**
 * Class FlexFormService
 *
 * @author Daniel Goerz <dlg@lightwerk.com>
 */
class FlexFormUserFunction implements SingletonInterface {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
	 */
	protected $objectManager;

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\EntryRepository
	 */
	protected $entryRepository;

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\CountryRepository
	 */
	protected $countryRepository;

	/**
	 * @var \Lightwerk\Vectormap\Domain\Repository\CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * @return void
	 */
	public function initializeObjects() {
		if (empty($this->objectManager)) {
			$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		}
		if (empty($this->markerRepository)) {
			$this->entryRepository = $this->objectManager->get('Lightwerk\Vectormap\Domain\Repository\EntryRepository');
		}
		if (empty($this->countryRepository)) {
			$this->countryRepository = $this->objectManager->get('Lightwerk\Vectormap\Domain\Repository\CountryRepository');
		}
		if (empty($this->categoryRepository)) {
			$this->categoryRepository = $this->objectManager->get('Lightwerk\Vectormap\Domain\Repository\CategoryRepository');
		}
	}

	/**
	 * @param array $config
	 * @return array
	 */
	public function getOptionsForCountrySelect(array $config) {
		$this->initializeObjects();
		$entries = $this->entryRepository->findAllInDatabase();
		$items = array();
		if ($entries->count() > 0) {
			$countries = $this->countryRepository->findAllByEntries($entries);
			foreach ($countries as $country) {
				$items[] = array(
					$country->getShortNameEn(),
					strtolower($country->getIsoCodeA2())
				);
			}
		}
		$config['items'] = $items;
		return $config;
	}

	/**
	 * @param array $config
	 * @return array
	 */
	public function getOptionsForCategorySelect(array $config) {
		$this->initializeObjects();
		$entries = $this->entryRepository->findAllInDatabase();
		$items = array();
		if ($entries->count() > 0) {
			$categories = $this->categoryRepository->findAllByEntries($entries);
			foreach ($categories as $category) {
				$items[] = array(
					$category->getTitle() . ' (' . $category->getParent()->getTitle() . ')',
					$category->getUid()
				);
			}
		}
		$config['items'] = $items;
		return $config;
	}

	/**
	 * @param array $config
	 * @return array
	 */
	public function getOptionsForContinentalSelect(array $config) {
		$this->initializeObjects();
		$items = array();
		foreach (ConfigurationService::$supportedContinentalRegions as $key => $supportedContinentalRegion) {
			$items[] = array($supportedContinentalRegion, $key);
		}
		$config['items'] = $items;
		return $config;
	}

	/**
	 * @param array $config
	 * @return array
	 */
	public function getOptionsForStartingPointSelect(array $config) {
		$items = array();
		$items[] = array(
			LocalizationUtility::translate('LLL:EXT:vectormap/Resources/Private/Language/locallang_flexform_pi1.xlf:startingPoint.1', 'vectormap'),
			ConfigurationService::STARTING_POINT_WORLD
		);
		$items[] = array(
			LocalizationUtility::translate('LLL:EXT:vectormap/Resources/Private/Language/locallang_flexform_pi1.xlf:startingPoint.2', 'vectormap'),
			ConfigurationService::STARTING_POINT_COUNTRY
		);
		$items[] = array(
			LocalizationUtility::translate('LLL:EXT:vectormap/Resources/Private/Language/locallang_flexform_pi1.xlf:startingPoint.3', 'vectormap'),
			ConfigurationService::STARTING_POINT_CONTINENTAL
		);
		$config['items'] = $items;
		return $config;
	}
}