<?php

namespace Lightwerk\Vectormap\UserFunction\Tca;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Xavier Perseguers <xavier@causal.ch>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\FormEngine;
use TYPO3\CMS\Lang\LanguageService;

/**
 * Google map.
 *
 * Source: http://xavier.perseguers.ch/tutoriels/typo3/articles/google-map-in-tca.html
 *
 * @package vectormap
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class GoogleMapUserFunction {

	/**
	 * Renders the Google map.
	 *
	 * @param array $PA
	 * @param FormEngine $pObj
	 * @return string
	 */
	public function render(array &$PA, FormEngine $pObj) {
		$latitude = (float) $PA['row']['latitude'];
		$longitude = (float) $PA['row']['longitude'];

		$baseElementId = isset($PA['itemFormElID']) ? $PA['itemFormElID'] : $PA['table'] . '_map';
		$jsBaseElementId = str_replace('.', '', $baseElementId);
		$mapId = $baseElementId . '_map';
		$markerAddressId = $baseElementId . '_markerAddress';

		if (!($latitude && $longitude)) {
			$latitude = 37.760908;
			$longitude = -122.400419;
		};

		$dataPrefix = 'data[' . $PA['table'] . '][' . $PA['row']['uid'] . ']';
		$latitudeField = $dataPrefix . '[latitude]';
		$longitudeField = $dataPrefix . '[longitude]';

		$updateJs = "TBE_EDITOR.fieldChanged('%s','%s','%s','%s');";
		$updateLatitudeJs = sprintf($updateJs, $PA['table'], $PA['row']['uid'], 'latitude', $latitudeField);
		$updateLongitudeJs = sprintf($updateJs, $PA['table'], $PA['row']['uid'], 'longitude', $longitudeField);

		return <<<EOT
<script type="text/javascript">
	if (typeof TxVectormap == 'undefined') TxVectormap = {};

	TxVectormap.$jsBaseElementId = {};

	TxVectormap.$jsBaseElementId.initialized = false;

	TxVectormap.$jsBaseElementId.init = function() {
		TxVectormap.$jsBaseElementId.origin = new google.maps.LatLng({$latitude}, {$longitude});
		var myOptions = {
			zoom: 13,
			center: TxVectormap.$jsBaseElementId.origin,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		TxVectormap.$jsBaseElementId.map = new google.maps.Map(document.getElementById("{$mapId}"), myOptions);
		TxVectormap.$jsBaseElementId.marker = new google.maps.Marker({
			map: TxVectormap.$jsBaseElementId.map,
			position: TxVectormap.$jsBaseElementId.origin,
			draggable: true
		});
		google.maps.event.addListener(TxVectormap.$jsBaseElementId.marker, 'dragend', function() {
			TxVectormap.$jsBaseElementId.updateCoordinates();
		});
		TxVectormap.$jsBaseElementId.geocoder = new google.maps.Geocoder();
		TxVectormap.$jsBaseElementId.setMarkerToCoordinates();
		TxVectormap.$jsBaseElementId.setMarkerAddressToInputField();

		// Make sure to refresh Google Map if corresponding tab is not yet active
		TxVectormap.$jsBaseElementId.tabPrefix = Ext.fly('{$mapId}').findParentNode('[id$="-DIV"]').id;
		TxVectormap.$jsBaseElementId.tabPrefix = Ext.util.Format.substr(TxVectormap.$jsBaseElementId.tabPrefix, 0, TxVectormap.$jsBaseElementId.tabPrefix.length - 4);
		if (Ext.fly(TxVectormap.$jsBaseElementId.tabPrefix + '-DIV').getStyle('display') == 'none') {
			Ext.fly(TxVectormap.$jsBaseElementId.tabPrefix + '-MENU').on('click', TxVectormap.$jsBaseElementId.refreshMap);
		}
		TxVectormap.$jsBaseElementId.initialized = true;
	};

	TxVectormap.$jsBaseElementId.refreshMap = function() {
		google.maps.event.trigger(TxVectormap.$jsBaseElementId.map, 'resize');
		// No need to do it again
		Ext.fly(TxVectormap.$jsBaseElementId.tabPrefix + '-MENU').un('click', TxVectormap.$jsBaseElementId.refreshMap);
	}

	TxVectormap.$jsBaseElementId.setMarker = function (address) {
		TxVectormap.$jsBaseElementId.geocoder.geocode({'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				TxVectormap.$jsBaseElementId.map.setCenter(results[0].geometry.location);
				TxVectormap.$jsBaseElementId.marker.setPosition(results[0].geometry.location);
				TxVectormap.$jsBaseElementId.setMarkerAddressToInputField();
				TxVectormap.$jsBaseElementId.updateCoordinates();
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	}

	TxVectormap.$jsBaseElementId.setMarkerToAddress = function () {
		var address = document[TBE_EDITOR.formname]['${dataPrefix}[street]_hr'].value + ' ' + document[TBE_EDITOR.formname]['${dataPrefix}[house_number]_hr'].value + ', ' +
			document[TBE_EDITOR.formname]['${dataPrefix}[zip_code]_hr'].value + ' ' + document[TBE_EDITOR.formname]['${dataPrefix}[city]_hr'].value + ', ' +
			document[TBE_EDITOR.formname]['${dataPrefix}[country]'][document[TBE_EDITOR.formname]['${dataPrefix}[country]'].selectedIndex].innerHTML;
		if (!TxVectormap.$jsBaseElementId.initialized) {
			TxVectormap.$jsBaseElementId.init();
		}
		TxVectormap.$jsBaseElementId.setMarker(address);
	}

	TxVectormap.$jsBaseElementId.setMarkerToCoordinates = function () {
		var address = document[TBE_EDITOR.formname]['{$latitudeField}_hr'].value + ',' + document[TBE_EDITOR.formname]['{$longitudeField}_hr'].value;
		if (address.length <= 1) {
			return;
		}
		TxVectormap.$jsBaseElementId.setMarker(address);
	}

	TxVectormap.$jsBaseElementId.updateCoordinates = function () {
		var lat = TxVectormap.$jsBaseElementId.marker.getPosition().lat().toFixed(6);
		var lng = TxVectormap.$jsBaseElementId.marker.getPosition().lng().toFixed(6);

		if (document[TBE_EDITOR.formname]['{$latitudeField}_hr'].value !== lat) {
			document[TBE_EDITOR.formname]['{$latitudeField}_hr'].value = lat;
			document[TBE_EDITOR.formname]['{$latitudeField}'].value = lat;
			{$updateLatitudeJs}
		}

		if (document[TBE_EDITOR.formname]['{$longitudeField}_hr'].value !== lng) {
			document[TBE_EDITOR.formname]['{$longitudeField}_hr'].value = lng;
			document[TBE_EDITOR.formname]['{$longitudeField}'].value = lng;
			{$updateLongitudeJs}
		}

		TxVectormap.$jsBaseElementId.setMarkerAddressToInputField();
	}

	TxVectormap.$jsBaseElementId.setMarkerAddressToInputField = function() {
		var latlng = new google.maps.LatLng(
			TxVectormap.$jsBaseElementId.marker.getPosition().lat(),
			TxVectormap.$jsBaseElementId.marker.getPosition().lng()
		);
		TxVectormap.$jsBaseElementId.geocoder.geocode({'latLng': latlng}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK && results[1]) {
				document.getElementById("{$markerAddressId}").innerHTML = results[0].formatted_address;
			}
		});
	}

	setTimeout(TxVectormap.$jsBaseElementId.init, 1500);
</script>
<div id="{$baseElementId}">
	<div>
		<input type="button" value="{$this->getTranslation('tx_vectormap_domain_model_marker.map.updateMapButton')}" onclick="TxVectormap.$jsBaseElementId.setMarkerToAddress();" />
	</div>
	<div id="{$mapId}" style="height:300px;width:550px;"></div>
	<div id="{$markerAddressId}" style="color:#555;">
		{$this->getTranslation('tx_vectormap_domain_model_marker.map.markerAddress.loading')}
	</div>
</div>
EOT;
	}

	/**
	 * @param string $key
	 * @return string
	 */
	protected function getTranslation($key) {
		return $this->getLanguageService()->sL('LLL:EXT:vectormap/Resources/Private/Language/locallang_db.xlf:' . $key);
	}

	/**
	 * @return LanguageService
	 */
	protected function getLanguageService() {
		return $GLOBALS['LANG'];
	}

}