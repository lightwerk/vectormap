<?php

namespace Lightwerk\Vectormap\UserFunction\Tca;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Xavier Perseguers <xavier@causal.ch>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\FormEngine;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Google map.
 *
 * Source: http://xavier.perseguers.ch/tutoriels/typo3/articles/google-map-in-tca.html
 *
 * @package vectormap
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class GoogleMapInclusionUserFunction {

	/**
	 * Renders the Google map.
	 *
	 * @param array $PA
	 * @param FormEngine $pObj
	 * @return string
	 */
	public function render(array &$PA, FormEngine $pObj) {
		$scheme = GeneralUtility::getIndpEnv('TYPO3_SSL') ? 'https' : 'http';
		return '<script type="text/javascript" src="' . $scheme . '://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>';
	}
}