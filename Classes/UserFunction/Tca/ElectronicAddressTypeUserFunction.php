<?php

namespace Lightwerk\Vectormap\UserFunction\Tca;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2011 Xavier Perseguers <xavier@causal.ch>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\FormEngine;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Lang\LanguageService;

/**
 * Electronic Address Type UserFunction
 *
 * @package vectormap
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License, version 3 or later
 */
class ElectronicAddressTypeUserFunction implements SingletonInterface {

	/**
	 * @var \Lightwerk\Vectormap\Service\ConfigurationService
	 * @inject
	 */
	protected $configurationService = NULL;

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
	 * @inject
	 */
	protected $objectManager = NULL;

	/**
	 * @return void
	 */
	public function initializeObject() {
		if (empty($this->objectManager)) {
			$this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		}
		if (empty($this->configurationService)) {
			$this->configurationService = $this->objectManager->get('Lightwerk\\Vectormap\\Service\\ConfigurationService');
		}
	}

	/**
	 * Renders the Google map.
	 *
	 * @param array $params
	 * @param FormEngine $parentObject
	 * @return void
	 */
	public function render(array $params, FormEngine $parentObject) {
		$this->initializeObject();

		$pageUid = $params['row']['pid'];
		$config = $this->configurationService->getModTSConfig($pageUid);
		$types = array();
		if (!empty($config['electronicAddress.']['types'])) {
			$types = GeneralUtility::trimExplode(',', $config['electronicAddress.']['types'], TRUE);
		}

		foreach ($types as $type) {
			$params['items'][] = array(
				$type,
				$this->getTranslation('tx_vectormap_domain_model_electronicaddress.type.items.' . strtolower($type)) ?: $type
			);
		}
	}

	/**
	 * @param string $key
	 * @return string
	 */
	protected function getTranslation($key) {
		return $this->getLanguageService()->sL('LLL:EXT:vectormap/Resources/Private/Language/locallang_db.xlf:' . $key);
	}

	/**
	 * @return LanguageService
	 */
	protected function getLanguageService() {
		return $GLOBALS['LANG'];
	}

}