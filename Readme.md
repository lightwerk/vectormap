Vectormap
=====

A vectormap Plugin for TYPO3 using [jvectormap](http://jvectormap.com/).

Dependencies
----
Jvectormap and the custom JavaScript depend on **jQuery** to work. Also **AngularJS** is required and shipped with this extension
 (version 1.2.27 for IE8 support, but vectormap should also work with the 1.3 branch). You can choose whether Angular
  should be included locally or globally in your site. Locally means it will be only on those pages with the vectormap
  plugin. Globally means ... globally. Or you can choose to disable the inclusion of AngularJS if you already got it
  on your site.
  
  The default configuration for including Angular is set to 'locally':  
  `plugin.tx_vectormap_pi1.settings.angular.enableLocally = 1`  
  `plugin.tx_vectormap_pi1.settings.angular.enableGlobally = 0`
  
  This extension also depends on **static_info_tables** in version 6.0 or higher, where we get some static data regarding
  countries.

Configuration
-----
The jvectormap can be configured in TypoScript constants (see plugin.tx_vectormap_pi1.settings.map.jsOptions).

The Frontend plugin(s) can be configured with the plugins flexforms. The main plugin provides the most configuration options
like defining the entry point of the map, restrict output to certain categories or enable or disable the filters as well as the map
or list.


Noteworthy
----
There are several GET-parameters to which the vectormap reacts. These are:

* `tx_vectormap[region]` - If this is a valid region key the map zooms to it
* `tx_vectormap[marker]` - If this is a valid marker uid, it is selected
* `tx_vectormap[category]` - If this is a valid category uid, it is selected


Unittests
----
To execute the unittests, make sure you have phpunit available on your system. Then go to the document root of
your TYPO3 and execute tests via `phpunit -c typo3conf/ext/vectormap/Build/UnitTests.xml`. The TYPO3 extension
phpunit is not required. Of course you can execute the tests with the phpunit extension via 
`./typo3/cli_dispatch.phpsh phpunit typo3conf/ext/vectormap` as well.