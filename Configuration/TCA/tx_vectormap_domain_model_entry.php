<?php

defined ('TYPO3_MODE') || exit('Access denied.');

$ll = 'LLL:EXT:vectormap/Resources/Private/Language/locallang_db.xlf:';
$table = 'tx_vectormap_domain_model_entry';

return array(
	'ctrl' => array(
		'title'	=> $ll . $table,
		'label' => 'name',
		'label_alt' => 'country',
		'label_alt_force' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,street,house_number,zip_code,city,country,salutation,information,images,categories,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('vectormap') . 'ext_icon.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, position, street, house_number, zip_code, city, country, markers, regions, electronic_addresses, salutation, information, images, categories, gmapinclude,',
	),
	'types' => array(
		'1' => array(
			'showitem' => 'name, position, street, house_number, zip_code, city, country, gmapinclude,' .
						  '--div--;' . $ll . 'tabs.' . $table . '.additionalInformation, electronic_addresses;;electronicAddressPalette, salutation, information;;;richtext:rte_transform[mode=ts_links],' .
						  '--div--;' . $ll . 'tabs.' . $table . '.marker, markers,regions,' .
						  '--div--;' . $ll . 'tabs.' . $table . '.relations, images, categories,' .
						  '--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1,'
		),
	),
	'palettes' => array(
		'1' => array(
			'showitem' => ''
		),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => TRUE,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => TRUE,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => $table,
				'foreign_table_where' => 'AND ' . $table . '.pid=###CURRENT_PID### AND ' . $table . '.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => TRUE,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'street' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.street',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'house_number' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.house_number',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'zip_code' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.zip_code',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'trim'
			)
		),
		'city' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'country' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.country',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array($ll . $table . '.country.items.0', 0),
				),
				'foreign_table' => 'static_countries',
				'foreign_table_where' => ' ORDER BY static_countries.cn_short_en',
				'size' => 1,
				'maxitems' => 1,
				'eval' => 'required'
			),
		),
		'electronic_addresses' => array(
			'exclude' => 1,
			'label' => $ll . $table . '.electronic_addresses',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_vectormap_domain_model_electronicaddress',
				'foreign_field' => 'entry',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 0,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'markers' => array(
			'exclude' => 1,
			'label' => $ll . $table . '.markers',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_vectormap_domain_model_marker',
				'foreign_field' => 'entry',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'regions' => array(
			'exclude' => 1,
			'label' => $ll . $table . '.regions',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_vectormap_domain_model_region',
				'foreign_field' => 'entry',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1
				),
			),
		),
		'salutation' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.salutation',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array($ll . $table . '.salutation.items.0', 0),
					array($ll . $table . '.salutation.items.1', 1),
					array($ll . $table . '.salutation.items.2', 2),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'information' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.information',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim',
				'wizards' => array(
					'RTE' => array(
						'icon' => 'wizard_rte2.gif',
						'notNewRecords'=> 1,
						'RTEonly' => 1,
						'module' => array(
							'name' => 'wizard_rte'
						),
						'title' => 'LLL:EXT:cms/locallang_ttc.xlf:bodytext.W.RTE',
						'type' => 'script'
					)
				)
			),
		),
		'position' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.position',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'images' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.images',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'images',
				array('maxitems' => 1),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'gmapinclude' => array(
			'label' => $ll . $table . '.map',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Lightwerk\\Vectormap\\UserFunction\\Tca\\GoogleMapInclusionUserFunction->render'
			),
		),
	),
);
