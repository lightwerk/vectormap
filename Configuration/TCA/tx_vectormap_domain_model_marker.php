<?php

defined ('TYPO3_MODE') || exit('Access denied.');

$ll = 'LLL:EXT:vectormap/Resources/Private/Language/locallang_db.xlf:';
$table = 'tx_vectormap_domain_model_marker';

return array(
	'ctrl' => array(
		'hideTable' => TRUE,

		'title'	=> $ll . $table,
		'label' => 'name',
		'label_alt' => 'country',
		'label_alt_force' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'name,street,house_number,zip_code,city,country,latitude,longitude,categories,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('vectormap') . 'ext_icon.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, street, house_number, zip_code, city, country, latitude, longitude',
	),
	'types' => array(
		'1' => array(
			'showitem' => 'name, street, house_number, zip_code, city, country, latitude, longitude, map,' .
						  '--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1,'
		),
	),
	'palettes' => array(
		'1' => array(
			'showitem' => ''
		),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => TRUE,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => TRUE,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => $table,
				'foreign_table_where' => 'AND ' . $table . '.pid=###CURRENT_PID### AND ' . $table . '.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => TRUE,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => TRUE,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'name' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.name',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'street' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.street',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'house_number' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.house_number',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'zip_code' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.zip_code',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'trim'
			)
		),
		'city' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'country' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.country',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array($ll . $table . '.country.items.0', 0),
				),
				'foreign_table' => 'static_countries',
				'foreign_table_where' => ' ORDER BY static_countries.cn_short_en',
				'size' => 1,
				'maxitems' => 1,
				'eval' => 'required'
			),
		),
		'map' => array(
			'label' => $ll . $table . '.map',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Lightwerk\\Vectormap\\UserFunction\\Tca\\GoogleMapUserFunction->render'
			),
		),
		'latitude' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.latitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		),
		'longitude' => array(
			'exclude' => TRUE,
			'label' => $ll . $table . '.longitude',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			)
		)
	)
);
