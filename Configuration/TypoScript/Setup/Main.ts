plugin.tx_vectormap {
	view {
		templateRootPath = {$plugin.tx_vectormap.view.templateRootPath}
		partialRootPath = {$plugin.tx_vectormap.view.partialRootPath}
		layoutRootPath = {$plugin.tx_vectormap.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_vectormap.persistence.storagePid}
		classes {
			Lightwerk\Vectormap\Domain\Model\Country.mapping.tableName = static_countries
			Lightwerk\Vectormap\Domain\Model\Category.mapping.tableName = sys_category
		}
	}
	features {
		rewrittenPropertyMapper = 1
	}
}

config.tx_extbase {
	persistence {
		classes {
			Lightwerk\Vectormap\Domain\Model\Country.mapping.tableName = static_countries
			Lightwerk\Vectormap\Domain\Model\Category.mapping.tableName = sys_category
		}
	}
}