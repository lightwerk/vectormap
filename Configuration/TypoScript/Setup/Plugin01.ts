plugin.tx_vectormap_pi1.settings {
	map {
		enable = {$plugin.tx_vectormap_pi1.settings.map.enable}
		markers {
			enable = {$plugin.tx_vectormap_pi1.settings.map.markers.enable}
		}
		nearbyMarkers {
			enable = {$plugin.tx_vectormap_pi1.settings.map.nearbyMarkers.enable}
			maximumDifference = {$plugin.tx_vectormap_pi1.settings.map.nearbyMarkers.maximumDifference}
		}
		height = {$plugin.tx_vectormap_pi1.settings.map.height}
		width = {$plugin.tx_vectormap_pi1.settings.map.width}
		jsOptions {
			map = mill_en
			startingPoint = 1
			zoomOnScroll = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.zoomOnScroll}
			zoomMax = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.zoomMax}
			markerStyle {
				initial {
					fill = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.markerStyle.initial.fill}
				}
				selected {
					fill = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.markerStyle.selected.fill}
				}
			}
			regionStyle {
				initial {
					fill = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.initial.fill}
					fill-opacity = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.initial.fill-opacity}
					stroke = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.initial.stroke}
					stroke-width = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.initial.stroke-width}
					stroke-opacity = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.initial.stroke-opacity}
				}
				hover {
					fill-opacity = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.hover.fill-opacity}
					cursor = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.hover.cursor}
				}
				selected {
					fill = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.selected.fill}
				}
				selectedHover {
					fill = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.regionStyle.selectedHover.fill}
				}
			}
			backgroundColor = {$plugin.tx_vectormap_pi1.settings.map.jsOptions.backgroundColor}
		}
	}
	searchFilter.enable = {$plugin.tx_vectormap_pi1.settings.searchFilter.enable}
	categoryFilter.enable = {$plugin.tx_vectormap_pi1.settings.categoryFilter.enable}
	countryFilter.enable = {$plugin.tx_vectormap_pi1.settings.countryFilter.enable}
	list {
		enable = {$plugin.tx_vectormap_pi1.settings.list.enable}
		hideOnStart = {$plugin.tx_vectormap_pi1.settings.list.hideOnStart}
		marker {
			image {
				maxWidth = {$plugin.tx_vectormap_pi1.settings.list.marker.image.maxWidth}
				maxHeight = {$plugin.tx_vectormap_pi1.settings.list.marker.image.maxHeight}
			}
			originalImage {
				maxWidth = {$plugin.tx_vectormap_pi1.settings.list.marker.originalImage.maxWidth}
				maxHeight = {$plugin.tx_vectormap_pi1.settings.list.marker.originalImage.maxHeight}
			}
		}
	}
	angular {
		enableLocally = {$plugin.tx_vectormap_pi1.settings.angular.enableLocally}
		enableGlobally = {$plugin.tx_vectormap_pi1.settings.angular.enableGlobally}
	}
}

page {
	includeCSS.jVectorMap = EXT:vectormap/Resources/Public/Vendor/jvectormap/jquery-jvectormap.css
	includeCSS.vectormap = EXT:vectormap/Resources/Public/Css/Vectormap.css
	includeJS.jQueryjVectorMap = EXT:vectormap/Resources/Public/Vendor/jvectormap/jquery-jvectormap.min.js
	includeJSFooter.vectormap = EXT:vectormap/Resources/Public/JavaScript/LIGHTWERK.vectormap.js
}

[globalVar = LIT: 1 = {$plugin.tx_vectormap_pi1.settings.angular.enableGlobally}]
	page.includeJSFooterlibs.angular = EXT:vectormap/Resources/Public/Vendor/angular/angular.js
[global]