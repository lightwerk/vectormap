plugin.tx_vectormap_pi1.settings {
	map {
		# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Map
		enable = 1
		markers {
			# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable display of markers on the map
			enable = 1
		}
		nearbyMarkers {
			# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Tracking of nearby Markers for combined tooltips
			enable = 1
			# cat=plugin.tx_vectormap/enable; type=boolean; label=Distance between two markers to made them "nearby". 1 Point of lat/long = 1000000
			maximumDifference = 5000
		}
		height = 400px
		width = 100%
		jsOptions {
			zoomOnScroll = true
			zoomMax = 30
			backgroundColor = #FFFFFF

			markerStyle {
				initial {
					fill = grey
				}
				selected {
					fill = red
				}
			}

			regionStyle {
				initial {
					fill = #999999
					fill-opacity = 1
					stroke = none
					stroke-width = 0
					stroke-opacity = 1
				}
				hover {
					fill-opacity = 0.8
					cursor = pointer
				}
				selected {
					fill = yellow
				}
				selectedHover {
					fill = orange
				}
			}
		}
	}
	# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Search Filter
	searchFilter.enable = 1
	# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Category Filter
	categoryFilter.enable = 1
	# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Country Filter
	countryFilter.enable = 1
	list {
		# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable List
		enable = 1
		# cat=plugin.tx_vectormap/enable; type=boolean; label=Hide List on Initialization
		hideOnStart = 0
		marker {
			image {
				maxWidth = 100
				maxHeight = 100
			}
			originalImage {
				maxWidth = 1024
				maxHeight = 1024
			}
		}
	}

	angular {
		# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Inclusion of AngularJS locally (only on Plugin Pages)
		enableLocally = 1
		# cat=plugin.tx_vectormap/enable; type=boolean; label=Enable Inclusion of AngularJS globally (turn both off, if you already have AngularJS on your site)
		enableGlobally = 0
	}
}