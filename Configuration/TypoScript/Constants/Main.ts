plugin.tx_vectormap {
	view {
		# cat=plugin.tx_vectormap/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:vectormap/Resources/Private/Templates/
		# cat=plugin.tx_vectormap/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:vectormap/Resources/Private/Partials/
		# cat=plugin.tx_vectormap/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:vectormap/Resources/Private/Layouts/
	}

	persistence {
		# cat=plugin.tx_vectormap//a; type=string; label=Default storage PID
		storagePid =
	}
}