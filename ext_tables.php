<?php

defined('TYPO3_MODE') || exit('Access denied.');

/**
 * Plugin Pi1
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Pi1',
	'Vector Map'
);
$pluginSignature = str_replace('_', '', $_EXTKEY) . '_pi1';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	$pluginSignature,
	'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/FlexformPi1.xml'
);

/**
 * Plugin Pi2
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Pi2',
	'Vector Map Select Boxes'
);
$pluginSignature = str_replace('_', '', $_EXTKEY) . '_pi2';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
	$pluginSignature,
	'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/FlexformPi2.xml'
);

/**
 * TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	$_EXTKEY,
	'Configuration/TypoScript',
	'Vector Map'
);

/**
 * SysCategories
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
	'vectormap',
	'tx_vectormap_domain_model_entry',
	'categories',
	array(
		'label' => 'LLL:EXT:vectormap/Resources/Private/Language/locallang_db.xlf:tx_vectormap_domain_model_marker.categories',
		'exclude' => TRUE,
		'fieldConfiguration' => array(
			'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.title ASC',
		)
	)
);

/**
 * Translations
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'tx_vectormap_domain_model_marker',
	'EXT:vectormap/Resources/Private/Language/locallang_csh_tx_vectormap_domain_model_marker.xlf'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'tx_vectormap_domain_model_electronicaddress',
	'EXT:vectormap/Resources/Private/Language/locallang_csh_tx_vectormap_domain_model_electronicaddress.xlf'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'tt_content.pi_flexform.vectormap_pi1.list',
	'EXT:vectormap/Resources/Private/Language/locallang_csh_flexform_pi1.xlf'
);